#!/usr/bin/env python

"""Benchmark the backtracking solve techniques."""


import re
import sys
import csv
import argparse
import logging
from timeit import default_timer as timer
from typing import Any, Final

import numpy as np

from sudoku.error import SudokuError
from sudoku.grid import Grid, Mode
from sudoku.solver import SolverMain
from sudoku.methods.finishers import Finisher, FinisherMethodType
from sudoku.methods.finishers import Backtracking1, Backtracking2
from sudoku.methods.finishers import Backtracking3, Backtracking4


PATTERN: Final[re.Pattern] = re.compile("^[0-9.]{81}$")


def quit_on_second_solution(finisher: Finisher,
                            user_data: list[str]) -> None:
    if finisher.solution_count == 1:
        user_data[0] = format(finisher.grid)

    if finisher.solution_count > 1:
        user_data[0] = "more than 1 solution"
        raise finisher.Breakout


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="benchmark.py",
                                     description="benchmark sudoku solvers")
    parser.add_argument("-v", "--verbose", action="store_true", help="""
                        verbose output in terminal if outfile is specified""")
    parser.add_argument("-m", "--method", action="store", default="4",
                        choices=("1", "2", "3", "4"),
                        help="""which method to test of Backtracking[1..4]""")
    parser.add_argument("infile", nargs="?", type=argparse.FileType("r"),
                        default=sys.stdin, help="""which test data to use""")
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.INFO, format="%(message)s")

    DType = dict[str, FinisherMethodType]
    finishers: DType = {"1": Backtracking1, "2": Backtracking2,
                        "3": Backtracking3, "4": Backtracking4}
    chosen_finisher = finishers[args.method]

    shortest_time = None
    total_time = 0.0
    longest_time = 0.0
    total_tests = 0
    most_stages = 0

    for line in csv.reader(args.infile):
        if len(line) != 1 or not PATTERN.match(line[0]):
            logging.info("bad test data")
        else:
            finisher = chosen_finisher(Grid.from_str(line[0]))
            finisher.set_output_callback(quit_on_second_solution)
            user_data = ["no solutions"]
            finisher.set_output_callback_user_data(user_data)
            start_t = timer()
            finisher()
            end_t = timer()
            elapsed = end_t - start_t
            total_time += elapsed
            total_tests += 1
            if elapsed > longest_time:
                longest_time = elapsed
            if shortest_time is None or shortest_time > elapsed:
                shortest_time = elapsed
            if finisher.stage_count > most_stages:
                most_stages = finisher.stage_count
            logging.info(f"{user_data[0]}")

    if total_tests:
        print(f"shortest time: {shortest_time}")
        print(f"average time: {total_time / total_tests}")
        print(f"longest time: {longest_time}")
        print(f"most stages: {most_stages}")
