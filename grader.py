#!/usr/bin/env python

"""Grade test data and output grids that conform to user criteria."""


import re
import sys
import csv
import argparse
import logging
from timeit import default_timer as timer
from typing import Final

import numpy as np

from sudoku.error import SudokuError
from sudoku.grid import Grid
from sudoku.solver import SolverMain
import sudoku.methods.core as core
import sudoku.methods.tough as tough


PATTERN: Final[re.Pattern] = re.compile("^[0-9.]{81}$")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="grader.py",
                                     description="""filter sudoku grids based
                                                    upon difficulty""")
    parser.add_argument("-v", "--verbose", action="store_true", help="""
                        enable warning messages""")
    grade = parser.add_mutually_exclusive_group()
    grade.required = True
    grade.add_argument("-0", "--newbie", action="store_true",
                       help="""magazine rack timewaster puzzle book""")
    grade.add_argument("-1", "--easy", action="store_true",
                       help="""maybe need pencil marks for this""")
    grade.add_argument("-2", "--medium", action="store_true",
                       help="""standard pencil techniques""")
    grade.add_argument("-3", "--tough", action="store_true",
                       help="""higher level pencil techniques e.g. Y wing""")
    grade.add_argument("-4", "--diabolical", action="store_true",
                       help="""hope you can spot chains""")
    parser.add_argument("infile", nargs="?", type=argparse.FileType("r"),
                        default=sys.stdin, help="""which grid data to use""")
    args = parser.parse_args()

    show_progress_counter = False
    if not sys.stdout.isatty():
        if args.verbose:
            logging.basicConfig(level=logging.INFO, format="%(message)s")
        else:
            show_progress_counter = True

    output = csv.writer(sys.stdout)
    try:
        for i, line in enumerate(csv.reader(args.infile), 1):
            if len(line) != 1 or not PATTERN.match(line[0]):
                logging.debug("bad test data: line {i}: data {line}")
                continue

            try:
                # If too_easy can solve it reject the grid.
                too_easy = SolverMain(Grid.from_str(line[0]))
                # If just_right can not solve it reject the grid.
                just_right = SolverMain(Grid.from_str(line[0]))
            except SudokuError as e:
                logging.warn(e)
                continue

            if args.newbie:
                # Grids can probably all be done by scanning.
                just_right.add_method(core.SinglesCleanup)
            elif args.easy:
                # No simple scanfest.
                too_easy.add_method(core.SinglesCleanup)
                # Grids may require NakedPairs.
                just_right.add_method(core.SinglesCleanup)
                just_right.add_method(core.HiddenSingles)
                just_right.add_method(core.NakedPairs)
            elif args.medium:
                # One or more higher core methods needed.
                too_easy.add_method(core.SinglesCleanup)
                too_easy.add_method(core.HiddenSingles)
                too_easy.add_method(core.NakedPairs)
                just_right.add_methods(core)
            elif args.tough:
                # At least one tough method required.
                too_easy.add_methods(core)
                just_right.add_methods(core)
                just_right.add_methods(tough)
            elif args.diabolical:
                # Beyond tough. Unsolvable by present means except finishers.
                # ToDo: add a module of even harder methods.
                too_easy.add_methods(core)
                too_easy.add_methods(tough)
                # No methods available for just_right.
                # By supplying none over look the test.
            try:
                too_easy.solve()
            except SudokuError as e:
                logging.debug(e)

            try:
                just_right.solve()
            except SudokuError as e:
                logging.debug(e)

            if too_easy.grid.filled == 81:
                logging.debug("too easy")
                continue

            if just_right.grid.filled < 81 and just_right.has_methods():
                logging.debug("too hard")
                continue

            output.writerow(line)
    except KeyboardInterrupt:
        pass
