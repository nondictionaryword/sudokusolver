#!/usr/bin/env python

"""Test a suite of human solve techniques against test data."""


import re
import sys
import csv
import argparse
import logging
from timeit import default_timer as timer
from typing import Any, Final

import numpy as np

from sudoku.error import SudokuError
from sudoku.grid import Grid, Mode
from sudoku.solver import SolverMain
from sudoku.methods import core, tough
from sudoku.methods.finishers import Finisher, FinisherMethodType
from sudoku.methods.finishers import Backtracking4


PATTERN: Final[re.Pattern] = re.compile("^[0-9.]{81}$")


def finisher_callback(finisher: Finisher, user_data: list[str]) -> None:
    if finisher.solution_count == 1:
        user_data[0] = format(finisher.grid)

    if finisher.solution_count > 1:
        user_data[0] = f"{finisher.solution_count} solution(s)"

    if finisher.solution_count > 10:
        user_data[0] = "more than 10 solutions"
        raise finisher.Breakout


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="solvtest.py",
                                     description="test human solve techniques")
    parser.add_argument("-v", "--verbose", action="store_true", help="""
                        verbose output in terminal if outfile is specified""")
    inex = parser.add_mutually_exclusive_group()
    inex.add_argument("-i", "--include", action="store", help="""
                      user may supply a comma separated solver name list""")
    inex.add_argument("-e", "--exclude", action="store", help="""
                      user may supply a comma separated solver name list""")
    parser.add_argument("-l", "--list", action="store_true", help="""
                        display a list of available solve methods for
                        include/exclude""")
    parser.add_argument("infile", nargs="?", type=argparse.FileType("r"),
                        default=sys.stdin, help="""which test data to use""")
    args = parser.parse_args()

    if args.list:
        print(",".join((core.SOLVE_METHODS + tough.SOLVE_METHODS)))
        exit(0)

    if args.verbose:
        logging.basicConfig(level=logging.INFO, format="%(message)s")

    total_filled_digits = 0

    for line in csv.reader(args.infile):
        if len(line) != 1 or not PATTERN.match(line[0]):
            logging.info("bad test data")
        else:
            grid = Grid.from_str(line[0])
            solver = SolverMain(grid)
            if args.include:
                inc = args.include.split(",")
                for method_name in inc:
                    if method_name in core.SOLVE_METHODS:
                        method = getattr(core, method_name)
                        solver.add_method(method)
                    elif method_name in tough.SOLVE_METHODS:
                        method = getattr(tough, method_name)
                        solver.add_method(method)
                    else:
                        print(f"unknown method {method_name}", file=sys.stderr)
                        exit(5)
            else:
                solver.add_methods(core)
                solver.add_methods(tough)
            if args.exclude:
                exc = args.exclude.split(",")
                for method_name in exc:
                    if method_name in core.SOLVE_METHODS:
                        method = getattr(core, method_name)
                        solver.remove_method(method)
                    elif method_name in tough.SOLVE_METHODS:
                        method = getattr(tough, method_name)
                        solver.remove_method(method)
                    else:
                        print(f"unknown method {method_name}", file=sys.stderr)
                        exit(5)

            finisher = Backtracking4(grid)
            finisher.set_output_callback(finisher_callback)
            finisher.set_output_callback_user_data([""])
            finisher()
            solution_count1 = finisher.solution_count
            solver.solve()
            user_data = ["no solutions"]
            finisher = Backtracking4(grid)
            finisher.set_output_callback(finisher_callback)
            finisher.set_output_callback_user_data(user_data)
            finisher()
            solution_count2 = finisher.solution_count

            if solution_count2 != solution_count1:
                raise SudokuError("solution count has changed")
            print(f"{grid:.}")
            total_filled_digits += grid.filled

    print(f"{total_filled_digits=}")
