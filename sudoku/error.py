"""Exceptions and debugging stuff."""


from __future__ import annotations

__all__ = ["SudokuError", "log"]

import logging
from contextlib import contextmanager


class SudokuError(Exception):
    """For reporting the grid being in a bad state."""


@contextmanager
def log(level: int | str):
    """For altering the logging level."""

    logger = logging.getLogger()
    current_level = logger.getEffectiveLevel()
    logger.setLevel(level)
    try:
        yield
    finally:
        logger.setLevel(current_level)
