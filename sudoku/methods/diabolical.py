"""Diabolical methods.

The pencil mark removing methods can help one another and for that reason
they can be written to do very simple things and work together as a team.

Some of these techniques are far from simple often requiring a separate
sheet of paper to try to fathom the various chains and loops.
"""

from __future__ import annotations

__all__ = ["XCycles"]

from typing import cast, Union, Optional, Final, NoReturn
import logging

import numpy as np

from . import PencilMethod
from .common import *
from ..grid import Grid
from ..error import SudokuError


SOLVE_METHODS: Final[list[str]] = __all__


class XCycles(PencilMethod):
    """Loops on a single digit alternating weakly/strongly.

    ToDo:
    Handles continuous loops (of even length) that eliminate on digits outside
    the loop like with simple coloring rule 4. Some/all of the weak links may
    be replaced with strong links.

    ToDo:
    Handles discontinuous loops (of odd length) that have a pair of conjoined
    weak links. The cell at the discontinuity if on will be indicated off
    after circling the loop. It can be removed.
    Some/all of the links (not the discontinuity) may be strong.

    ToDo:
    Handles discontinuous loops (of odd length) that have a pair of conjoined
    strong links. The cell at the discontinuity is the solution.
    """

    DEPENDENCIES = ["SinglesCleanup"]

    class Breakout(Exception):
        pass

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.other_marks: Optional[np.ndarray] = None

    def __call__(self) -> int:
        logging.debug("* X Cycles *")

        grid = self.grid
        prior_grid = grid.grid.copy()
        made_progress = False

        try:
            for mark_number in range(1, 10):
                logging.debug(f"{mark_number=}")
                mark = 1 << mark_number
                self.other_marks = grid.grid & ~mark
                grid.grid[:] &= mark
                cells = list(zip(*np.nonzero(np.logical_and(
                             grid.grid, self.other_marks))))
                self.strong_links = get_strong_links(grid)
                try:
                    while cells:
                        self._process(cells, cells[0][0], cells[0][1])
                        cells.pop(0)
                finally:
                    np.copyto(grid.grid, grid.grid | self.other_marks)
        except self.Breakout:
            pass

        return np.flatnonzero(grid.grid != prior_grid).size

    def _process(self, cells: list[tuple[int, int]],
                 cur_row: int, cur_col: int,
                 path: tuple[tuple[int, int], ...] = (),
                 strengths: str = "",
                 row_links: int = 0, col_links: int = 0, box_links: int = 0,
                 depth: int = 1) -> None:

        if strengths.endswith("www"):
            return

        if strengths.endswith("ww") and "ww" in strengths.rstrip("w"):
            return

        if depth >= 5 and cur_row == cells[0][0] and cur_col == cells[0][1]:
            if "www" in strengths + strengths:
                return

            print("loop achieved")
            breakpoint()

        strong_links = get_strong_links(self.grid)
        try:
            conjugates = strong_links[(cur_row, cur_col)].conjugates
        except KeyError:
            conjugates = None

        for row, col in cells:
            box = 1 << row - row % 3 + col // 3

            # Determine the link type.
            on_row = row == cur_row
            on_col = col == cur_col
            if on_row and on_col:  # Reject b/c it's the same cell.
                continue
            on_box = row // 3 == cur_row // 3 and col // 3 == cur_col // 3

            if not (on_row or on_col or on_box):  # Reject b/c cell unrelated.
                continue

            # Repetitive but this code needs to run fast.
            if on_row:
                new_row_links = row_links | 1 << row
                if new_row_links == row_links:
                    # Reject b/c previously used row.
                    continue
            else:
                new_row_links = row_links
            if on_col:
                new_col_links = col_links | 1 << col
                if new_col_links == col_links:
                    # Reject b/c prevously used column.
                    continue
            else:
                new_col_links = col_links
            if on_box:
                new_box_links = box_links | 1 << box
                if new_box_links == box_links:
                    # Reject b/c previously used box.
                    continue
            else:
                new_box_links = box_links

            if conjugates is not None and (row, col) in conjugates:
                strength = "s"
            else:
                strength = "w"
                if on_box and not on_row and not on_col:
                    strength = "b"  # Nail this down later.

            self._process(cells, row, col,
                          path + ((cur_row, cur_col), ),
                          strengths=strengths + strength,
                          row_links=new_row_links,
                          col_links=new_col_links,
                          box_links=new_box_links,
                          depth=depth + 1)





