"""The core methods.

The pencil mark removing methods can help one another and for that reason
they can be written to do very simple things and work together as a team.

Higher techniques will not complete a grid but these often can."""


from __future__ import annotations

__all__ = ["SinglesCleanup", "HiddenSingles", "NakedPairs", "NakedTriples",
           "HiddenPairs", "HiddenTriples", "NakedQuads", "HiddenQuads",
           "PointingPairs", "BoxLineReduction"]

import logging
import itertools
from functools import reduce
from operator import __or__ as or_
from collections.abc import Iterable
from typing import Any, Iterable, Final

import numpy as np

from . import PencilMethod
from ..error import SudokuError
from ..grid import Grid


SOLVE_METHODS: Final[list[str]] = __all__


class SinglesCleanup(PencilMethod):
    """Mark elimination occurs on row, column, and box of single value cells.

    Methods that create single value cells should assume that this is
    going to be in the solver rotation. Ideally this is always the first method
    in the list."""

    def __call__(self) -> int:
        logging.debug("** Singles Cleanup **")

        counter = 0
        grid = self.grid
        for mark_number in range(1, 10):
            mark = 1 << mark_number
            logging.debug(f"targetting solo {mark_number} marks")
            rows, cols = np.nonzero(grid.grid == mark)
            if not rows.size:
                logging.debug("none found")
                continue

            specific_counter = 0
            prior_grid = grid.grid.copy()
            for row, col in zip(rows, cols):
                self._purge_mark_from(grid.rows[row], mark)
                self._purge_mark_from(grid.cols[col], mark)
                self._purge_mark_from(grid.box_at_cell(row, col), mark)
                grid.grid[row, col] = mark  # Restore the mark.

            specific_counter = np.flatnonzero(grid.grid != prior_grid).size
            counter += specific_counter
            logging.debug(f"eliminated {specific_counter} associated "
                          f"{mark_number} marks")

        return counter

    @staticmethod
    def _purge_mark_from(group: np.ndarray, mark: int) -> None:
        group &= ~mark


class HiddenSingles(PencilMethod):
    """Hidden singles are marks in busy cells that appear only once in a group.

    Other pencil marks in such cells are removed unhiding them."""

    def __call__(self) -> int:
        logging.debug("** Hidden Singles **")

        counter = 0
        grid = self.grid
        for mark_number in range(1, 10):
            mark = 1 << mark_number
            logging.debug(f"targetting hidden {mark_number} marks")
            specific_counter = 0
            prior_grid = grid.grid.copy()
            for row, col, box in zip(grid.rows, grid.cols, grid.boxes.flat):
                self._reveal_single(row, mark)
                self._reveal_single(col, mark)
                self._reveal_single(box.flat, mark)

            specific_counter = np.flatnonzero(grid.grid != prior_grid).size
            counter += specific_counter
            if specific_counter:
                logging.debug(f"{specific_counter} found")
            else:
                logging.debug("no progress")

        return counter

    def _reveal_single(self, group: Any, mark: int) -> None:
        marked = np.flatnonzero(group.copy() & mark)

        if marked.size == 0:
            raise SudokuError("double hidden single or missing mark")
        if marked.size == 1:
            group[marked] = mark


class NakedPairs(PencilMethod):
    """Naked pairs are pairs of candidates that appear twice in a group.

    Those candidates are therefore locked to that particular pair of cells
    allowing pencil marks to be cleaned up."""

    def __call__(self) -> int:
        logging.debug("** Naked Pairs **")

        grid = self.grid
        prior_grid = grid.grid.copy()
        aspects = zip(grid.rows, grid.cols, grid.boxes.flat)
        for i, (row, col, box) in enumerate(aspects):
            self._process(row, f"row {grid.ROW_NAMES[i]}")
            self._process(col, f"column {grid.COLUMN_NAMES[i]}")
            self._process(box.flat, f"box {i + 1}")

        return np.flatnonzero(grid.grid != prior_grid).size

    def _process(self, group: Any, specifier: str) -> None:
        group_copy = group.copy()
        pairs = np.unique(np.extract(np.isin(group, self.grid.PAIRS), group))

        for pair in pairs:
            indices = np.flatnonzero(group_copy & ~pair)
            if indices.size == 7:
                group[indices] &= ~pair
                if (group != group_copy).any():
                    group_copy = group.copy()
                    logging.debug(f"pair of {self.grid.pencil_values(pair)} "
                                  f"cleans {specifier}")


class HiddenPairs(PencilMethod):
    """Hidden pairs only appear twice and together in a group.

    Those candidates are therefore locked to that particular pair of cells
    allowing pencil marks to be cleaned up creating a naked pair."""

    def __call__(self) -> int:
        logging.debug("** Hidden Pairs **")

        grid = self.grid
        self.SINGLES = grid.SINGLES
        prior_grid = grid.grid.copy()
        aspects = zip(grid.rows, grid.cols, grid.boxes.flat)
        for i, (row, col, box) in enumerate(aspects):
            self._process(row, f"row {grid.ROW_NAMES[i]}")
            self._process(col, f"column {grid.COLUMN_NAMES[i]}")
            self._process(box.flat, f"box {i + 1}")

        return np.flatnonzero(grid.grid != prior_grid).size

    def _process(self, group: Any, specifier: str) -> None:
        group_copy = group.copy()
        # obtain a list of pencil marks that appear twice in a group
        # much faster than iterating over the entire self.grid.PAIRS
        number_stack = np.vstack([group_copy & x for x in self.SINGLES])
        vals, counts = np.unique(number_stack, return_counts=True)
        duos = np.extract(np.logical_and(np.logical_and(
                                         counts > 1, counts < 3), vals), vals)

        for value_1, value_2 in itertools.combinations(duos, 2):
            pair = value_1 | value_2
            indices = np.flatnonzero(group_copy & pair)
            if indices.size == 2:
                group[indices] &= pair
                if (group != group_copy).any():
                    group_copy = group.copy()
                    logging.debug(f"hidden {self.grid.pencil_values(pair)} "
                                  f"pair found in {specifier} {indices + 1}")


class NakedTriples(PencilMethod):
    """Naked triples are three values residing on three cells in a group.

    Those candidates are therefore locked to those particular cells
    allowing pencil marks to be cleaned up."""

    def __call__(self) -> int:
        logging.debug("** Naked Triples **")

        grid = self.grid
        prior_grid = grid.grid.copy()
        aspects = zip(grid.rows, grid.cols, grid.boxes.flatten())
        for i, (row, col, box) in enumerate(aspects):
            self._process(row, f"row {grid.ROW_NAMES[i]}")
            self._process(col, f"column {grid.COLUMN_NAMES[i]}")
            self._process(box.flat, f"box {i + 1}")

        return np.flatnonzero(grid.grid != prior_grid).size

    def _process(self, group: np.ndarray, specifier: str) -> None:
        group_copy = group.copy()

        # NumPy v2 has a bitwise_count but this will do for now.
        count = self.bitwise_count
        bits = reduce(or_, (x for x in group if count(x) in range(2, 4)), 0)
        for values in itertools.combinations(self.iterbits(bits), 3):
            combo = reduce(or_, values)
            indices = np.flatnonzero(group_copy & ~combo)
            if indices.size == 6:
                group[indices] &= ~combo
                if (group != group_copy).any():
                    group_copy = group.copy()
                    logging.debug(f"combo of {self.grid.pencil_values(combo)} "
                                  f"cleans {specifier}")


class HiddenTriples(PencilMethod):
    """Hidden triples are three values distributed into three cells.

    Those candidates are therefore locked to that particular trio of cells
    allowing pencil marks to be cleaned up creating a naked triple."""

    def __call__(self) -> int:
        logging.debug("** Hidden Triples **")

        grid = self.grid
        self.SINGLES = grid.SINGLES
        prior_grid = grid.grid.copy()
        aspects = zip(grid.rows, grid.cols, grid.boxes.flat)
        for i, (row, col, box) in enumerate(aspects):
            self._process(row, f"row {grid.ROW_NAMES[i]}")
            self._process(col, f"column {grid.COLUMN_NAMES[i]}")
            self._process(box.flat, f"box {i + 1}")

        return np.flatnonzero(grid.grid != prior_grid).size

    def _process(self, group: Any, specifier: str) -> None:
        group_copy = group.copy()
        # Find the set of all pencil marks to be found within the bi-value
        # and tri-value cells of a group.
        # Much faster than iterating over self.grid.TRIPLES
        number_stack = np.vstack([group_copy & x for x in self.SINGLES])
        vals, counts = np.unique(number_stack, return_counts=True)
        trios = np.extract(np.logical_and(np.logical_and(
                                          counts > 1, counts < 4), vals), vals)

        for value_1, value_2, value_3 in itertools.combinations(trios, 3):
            combo = value_1 | value_2 | value_3
            indices = np.flatnonzero(group_copy & combo)
            if indices.size == 3:
                group[indices] &= combo
                if (group != group_copy).any():
                    group_copy = group.copy()
                    logging.debug(f"hidden {self.grid.pencil_values(combo)} "
                                  f"triple found in {specifier} {indices + 1}")


class NakedQuads(PencilMethod):
    """Naked quads are four values residing naked on four cells.

    Those candidates are therefore locked to those particular cells
    allowing pencil marks to be cleaned up."""

    def __call__(self) -> int:
        logging.debug("** Naked Quads **")

        grid = self.grid
        prior_grid = grid.grid.copy()
        aspects = zip(grid.rows, grid.cols, grid.boxes.flatten())
        for i, (row, col, box) in enumerate(aspects):
            self._process(row, f"row {grid.ROW_NAMES[i]}")
            self._process(col, f"column {grid.COLUMN_NAMES[i]}")
            self._process(box.flat, f"box {i + 1}")

        return np.flatnonzero(grid.grid != prior_grid).size

    def _process(self, group: np.ndarray, specifier: str) -> None:
        group_copy = group.copy()

        count = self.bitwise_count
        bits = reduce(or_, (x for x in group if count(x) in range(2, 5)), 0)
        for values in itertools.combinations(self.iterbits(bits), 4):
            combo = reduce(or_, values)
            indices = np.flatnonzero(group_copy & ~combo)
            if indices.size == 5:
                group[indices] &= ~combo
                if (group != group_copy).any():
                    group_copy = group.copy()
                    logging.debug(f"combo of {self.grid.pencil_values(combo)} "
                                  f"cleans {specifier}")


class HiddenQuads(PencilMethod):
    """Hidden quads are four values limited to four cells among other values.

    Those candidates are therefore locked to that particular four cells
    allowing pencil marks to be cleaned up creating a naked quad."""

    def __call__(self) -> int:
        logging.debug("** Hidden Quads **")

        grid = self.grid
        self.SINGLES = grid.SINGLES
        prior_grid = grid.grid.copy()
        aspects = zip(grid.rows, grid.cols, grid.boxes.flat)
        for i, (row, col, box) in enumerate(aspects):
            self._process(row, f"row {grid.ROW_NAMES[i]}")
            self._process(col, f"column {grid.COLUMN_NAMES[i]}")
            self._process(box.flat, f"box {i + 1}")

        return np.flatnonzero(grid.grid != prior_grid).size

    def _process(self, group: Any, specifier: str) -> None:
        group_copy = group.copy()
        # Find the set of all pencil marks to be found within the bi-value
        # tri-value, and quad-value cells of a group.
        # Vastly quicker than iterating over self.grid.QUADS.
        number_stack = np.vstack([group_copy & x for x in self.SINGLES])
        vals, counts = np.unique(number_stack, return_counts=True)
        quads = np.extract(np.logical_and(np.logical_and(
                                          counts > 1, counts < 5), vals), vals)

        for values in itertools.combinations(quads, 4):
            combo = values[0] | values[1] | values[2] | values[3]
            indices = np.flatnonzero(group_copy & combo)
            if indices.size == 4:
                group[indices] &= combo
                if (group != group_copy).any():
                    group_copy = group.copy()
                    logging.debug(f"hidden {self.grid.pencil_values(combo)} "
                                  f"triple found in {specifier} {indices + 1}")


class PointingPairs(PencilMethod):
    """Pointing pairs/triples place the candidate on some row or column.

    When all marks of a particular value in a box are aligned on the same
    row or column they eliminate marks on the rest of the row or column in
    related boxes."""

    def __call__(self) -> int:
        logging.debug("** Pointing Pairs **")

        total_progress = 0
        grid = self.grid
        for mark_number in range(1, 10):
            mark = 1 << mark_number
            progress = 0
            logging.debug(f"targetting pointing pairs of number {mark_number}")

            for box_row, box_col in itertools.product(range(3), repeat=2):
                box = grid.boxes[box_row, box_col]
                rows, cols = np.nonzero(box & mark)

                if rows.size in (2, 3) and rows.min() == rows.max():
                    prior_grid = grid.grid.copy()
                    row = grid.rows[box_row*3 + rows[0]]
                    row &= ~mark
                    for row, col in zip(rows, cols):
                        box[row, col] |= mark
                    progress += np.flatnonzero(grid.grid != prior_grid).size

                elif cols.size in (2, 3) and cols.min() == cols.max():
                    prior_grid = grid.grid.copy()
                    col = grid.cols[box_col*3 + cols[0]]
                    col &= ~mark
                    for row, col in zip(rows, cols):
                        box[row, col] |= mark
                    progress += np.flatnonzero(grid.grid != prior_grid).size

            if progress:
                logging.debug(f"{progress} marks removed")
                total_progress += progress

        return total_progress


class BoxLineReduction(PencilMethod):
    """Eliminate pencil marks from two lines in a box.

    This happens whenever the spared line contains the only candidates on
    the exteneded spared line that encompases three boxes."""

    def __call__(self) -> int:
        grid = self.grid
        prior_grid = grid.grid.copy()
        for band in itertools.chain(grid.row_boxes, grid.col_boxes):
            self._process(band)

        return np.flatnonzero(grid.grid != prior_grid).size

    def _process(self, boxes: tuple[np.ndarray, ...]) -> None:
        for pencil_mark in self.grid.SINGLES:
            boxes_ = [x & pencil_mark for x in boxes]
            inverse_mark = ~pencil_mark

            for bi, li in itertools.product(range(3), repeat=2):
                # Complementary indices.
                bi1, bi2 = set(range(3)) - set([bi])
                li1, li2 = set(range(3)) - set([li])

                if boxes_[bi][li].any() and (not boxes_[bi1][li].any() and
                                             not boxes_[bi2][li].any()):
                    boxes[bi][li1] &= inverse_mark
                    boxes[bi][li2] &= inverse_mark
