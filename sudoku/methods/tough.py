"""Tough methods.

The pencil mark removing methods can help one another and for that reason
they can be written to do very simple things and work together as a team.

The tough techniques live here but really the toughness is in the spotting."""


from __future__ import annotations

__all__ = ["XWing", "SimpleColoring", "YWing", "RectangleElimination",
           "Swordfish", "XYZWing", "BUG"]

from typing import cast, Union, Optional, Final, NoReturn
from collections import defaultdict
from dataclasses import dataclass
import itertools
import logging

import numpy as np

from . import PencilMethod, MarkCheck
from ..grid import Grid
from ..error import SudokuError
from .common import *
from .core import SinglesCleanup


SOLVE_METHODS: Final[list[str]] = __all__


class XWing(PencilMethod):
    """X-wing solver.

    Does not handle cases involving boxes so use it in conjunction with
    core.PointingPairs and core.BoxLineReduction.

    For a given digit scan an axis looking for lines that have exactly two
    places for that digit. When found look for another similar line where the
    digits line up. These cells form the X wing. On the other axis through
    those cells but not in the cells themselves the digit under test can be
    removed."""

    def __call__(self) -> int:
        grid = self.grid
        prior_grid = grid.grid.copy()

        for mark_number in range(1, 10):
            mark = 1 << mark_number
            if self._process(mark, search=grid.rows & mark, clean=grid.cols):
                break
            if self._process(mark, search=grid.cols & mark, clean=grid.rows):
                break

        return np.flatnonzero(grid.grid != prior_grid).size

    @staticmethod
    def _process(mark: int, search: np.ndarray, clean: np.ndarray) -> bool:
        """Scan search axis then where appropriate clean on the clean axis."""

        not_mark = ~mark
        lines: dict[tuple[int, int], int] = {}
        for line_number, line in enumerate(search):
            found = np.flatnonzero(line)
            if found.size == 2:
                positions = cast(tuple[int, int], tuple(found))
                try:
                    other_line_number = lines[positions]
                except KeyError:
                    lines[positions] = line_number
                else:
                    stored = clean.copy()
                    for pos in positions:
                        clean[pos] &= not_mark
                        for line in line_number, other_line_number:
                            clean[pos, line] |= mark
                    if (clean != stored).any():
                        return True
        return False


class SimpleColoring(PencilMethod):
    """A.K.A. Single's Chains

    One digit at a time hence the term simple let us make a chain of conjugate
    pairs spanning rows, cols, and boxes.
    Colorize them alternately then look for inconsitencies.

    From sudokuwiki.com we shall use rule 2 and rule 4, a subset of a set
    of rules for handling multi-value coloring. Here we consider just one
    value at a time hence the name Simple Coloring.

    Rule 2: Same color cells on the same chain that can see one another
    indicates a logical inconsistency therefore all of that color on the chain
    can be removed.

    Rule 4: Cells outside the chain that can see chain members of both colors
    can be removed."""

    def __call__(self) -> int:
        logging.debug("* Simple Coloring *")

        grid = self.grid
        prior_grid = grid.grid.copy()
        made_progress = False

        # Filter grid on mark to make processing simpler.
        for mark_number in range(1, 10):
            logging.debug(f"{mark_number=}")
            mark = 1 << mark_number
            other_marks = grid.grid & ~mark
            grid.grid[:] &= mark
            made_progress = self._process(mark)
            np.copyto(grid.grid, grid.grid | other_marks)
            if made_progress:
                break

        return np.flatnonzero(grid.grid != prior_grid).size

    def _process(self, mark: int) -> bool:
        grid = self.grid
        for chain in extract_chain(get_strong_links(grid)):
            logging.debug(f"{chain=}")

            # Rule 2: detect links of a color appearing in own influence map
            for color in Color.RED, Color.BLUE:
                link_map = chain.link_map[color]
                if (link_map & chain.influence_map[color]).any():
                    np.copyto(grid.grid, 0, where=link_map)
                    return True

            # Rule 4: wipe cells where influence maps intersect
            stored = grid.grid.copy()
            map_ = chain.influence_map
            np.copyto(grid.grid, 0, where=map_[Color.RED] & map_[Color.BLUE])
            if (grid.grid != stored).any():
                return True

        return False


class YWing(PencilMethod):
    """Y-wing solver.

    Let a, b, and c be three different pencil values.
    For a cell with values a,b find wings of a,c and b,c.
    One of the wings must solve to c so we can eliminate
    the c value from any cells seen by both wings together."""

    def __call__(self) -> int:
        logging.debug("* Y Wing *")

        grid = self.grid.grid
        prior_grid = grid.copy()

        hinges = np.nonzero(np.isin(grid, self.grid.PAIRS))

        for row, col in zip(*hinges):
            hinge_vals = Grid.pencil_values(grid[row, col])
            hinge_sees = point_influence(row, col)
            for guest_val in set(range(1, 10)) - set(hinge_vals):
                wing_red_val = (1 << hinge_vals[0]) | (1 << guest_val)
                wing_blue_val = (1 << hinge_vals[1]) | (1 << guest_val)
                wing_reds = cast(pos_2d, np.nonzero((grid == wing_red_val)
                                 & hinge_sees))
                wing_blues = cast(pos_2d,
                                  np.nonzero((grid == wing_blue_val)
                                             & hinge_sees))
                if wing_reds[0].size and wing_blues[0].size:
                    red_map = points_influence(wing_reds)
                    blue_map = points_influence(wing_blues)
                    np.bitwise_and(grid, ~(1 << guest_val), out=grid,
                                   where=red_map & blue_map)
                    if progress := np.flatnonzero(grid != prior_grid).size:
                        return progress
        return 0


class RectangleElimination(PencilMethod):
    """Eliminate digits from cells that if set would result in an empty box.

    The pattern takes the form of a rectangle at whose corners are a box
    that is vulnerable to overelimination, a cell under test (wing2) that if
    asserted would rid the box of all instances of the current digit under
    test, and a strongly linked pair of cells most important of which is
    wing1 which also sees the box. Wing2 implies wing1 and if both are needed
    to rid the box of all instances of the digit under test then wing2 can be
    removed. If wing2 is hard linked to the hinge cell then wing1 can go too.

    The action of both wings is required which places some pattern restrictions
    on the box. The cells need to be either an offset pair or in some kind of
    L, T, or + pattern with maybe the odd cell missing.
    """

    def __call__(self) -> int:
        logging.debug("* Rectangle Elimination *")
        grid = self.grid
        progress: int = 0

        # Filter grid on mark to make processing simpler.
        for mark_number in range(1, 10):
            logging.debug(f"{mark_number=}")
            mark = 1 << mark_number
            other_marks = grid.grid & ~mark
            grid.grid[:] &= mark
            progress = self._process(grid.rows) or self._process(grid.cols)
            np.copyto(grid.grid, grid.grid | other_marks)
            if progress:
                return progress

        return 0

    def _process(self, grid: np.ndarray) -> int:
        """Scanning for hard links.

        The grid is passed in straight and transposed so that row and column
        based hard links can be found by the same code.
        Each end of a hard link takes turns being the hinge cell.
        """

        for row_num, row_data in enumerate(grid):
            cols = np.flatnonzero(row_data)
            if cols.size == 2:  # Value of 2 indicates presence of a hard link.
                for each in (iter(cols), reversed(cols)):
                    progress = self._process_2(grid, row_num, *each)
                    if progress:
                        return progress
        return 0

    @staticmethod
    def _box_intersections(grid: np.ndarray,
                           band: int, stack: int) -> list[tuple[int, int]]:
        """Return the intersection points on the indicated box.

        An offset pair of cells has two intersection points while other forms
        (like +, L, T) have one. If two lines on opposing axes can cover all
        the candidates for a number and it requires two lines to do it then
        it counts.
        """

        results: Final[list[tuple[int, int]]] = []

        def pad(box: np.ndarray) -> None:
            """Fill out broken rows and columns to reduce the problem size."""

            for line in box:
                if np.count_nonzero(line) == 2:
                    line[:] = True

        def line_index(box: np.ndarray) -> int | NoReturn:
            """Returns the index of the first line that is found to be full."""

            for index, line in enumerate(box):
                if line.all():
                    return index
            raise SudokuError("never call this if a full line is not there")

        # Make a 3x3 boolean grid of the box data that we can safely alter.
        box_top = band * 3
        box_left = stack * 3
        box = grid[box_top: box_top + 3, box_left: box_left + 3] > 0
        transposed_box = np.transpose(box)
        # Fill in broken lines.
        pad(box)
        pad(transposed_box)
        pad(box)

        rows, cols = box.nonzero()
        cell_count = rows.size

        if cell_count == 0:
            raise SudokuError("box is missing marks")
        elif cell_count == 2:
            # The cells are guaranteed to be offset thanks to the processing.
            # The intersection points are at opposite corners to the cells.
            results.append((rows[0] + box_top, cols[1] + box_left))
            results.append((rows[1] + box_top, cols[0] + box_left))
        elif cell_count == 5:
            # A single intersection point is guaranteed.
            results.append((line_index(box) + box_top,
                            line_index(transposed_box) + box_left))
        return results

    def _process_2(self, grid: np.ndarray, row: int, wing1: int, hinge: int
                   ) -> int:
        """Condensing a thousand words into a picture:

                [box ]               <wing2
                --------8<-------8<--------
                ^wing1 =(hard link)= hinge^
        """

        progress = 0

        # Both wing1 and hinge need to be in different boxes for the pattern.
        if wing1 // 3 == hinge // 3:
            return 0

        wing2s = np.flatnonzero(grid[:, hinge])
        intersections: list[tuple[int, int]] = []
        band: int = -1
        for wing2 in wing2s:
            # There can be no wing2's in the same box as the hinge.
            if wing2 // 3 == row // 3:
                continue
            # Update the intersections value if and when needed.
            if band != wing2 // 3:
                band = wing2 // 3
                intersections = self._box_intersections(grid, band, wing1 // 3)

            for inter_row, inter_col in intersections:
                if inter_col != wing1 or inter_row != wing2:
                    continue

                logging.debug(grid)
                logging.debug(f"{row=} {hinge=} {wing1=} {wing2=}")

                grid[wing2, hinge] = 0
                progress += 1
                if wing2s.size == 2:
                    # Double hard link so both wings can go.
                    grid[row, wing1] = 0
                    progress += 1

                logging.debug(grid)
                break

        return progress


class Swordfish(PencilMethod):
    """Does perfect and imperfect swordfish but not the 'fin' variants.

    In an 'imperfect', a cell from a different column can be excluded on
    each line of the swordfish for example:

                   x    x
              x    x
              x         x
    """

    def __call__(self) -> int:
        logging.debug("* Swordfish *")

        grid = self.grid
        prior_grid = grid.grid.copy()

        for mark_number in range(1, 10):
            mark = 1 << mark_number
            logging.debug(f"checking digit {mark_number} on rows")
            if self._process(mark, search=grid.rows & mark, clean=grid.cols):
                break
            logging.debug(f"checking digit {mark_number} on columns")
            if self._process(mark, search=grid.cols & mark, clean=grid.rows):
                break

        return np.flatnonzero(grid.grid != prior_grid).size

    @staticmethod
    def _process(mark: int, search: np.ndarray, clean: np.ndarray) -> bool:

        not_mark = ~mark
        potentials: dict[int, tuple[int, ...]] = {}

        # Build a dictionary of swordfish candidate lines.
        for line_number, line in enumerate(search):
            indices: tuple[int, ...] = tuple(np.flatnonzero(line))
            if len(indices) in (2, 3):
                potentials[line_number] = indices
                logging.debug(f"potential swordfish "
                              f"{line_number=} {indices=}")

        for a, b, c in itertools.combinations(potentials.items(), 3):
            set_ixa = set(a[1])
            set_ixb = set(b[1])
            set_ixc = set(c[1])
            # Check for correct overall size.
            combined_set = set_ixa | set_ixb | set_ixc
            if len(combined_set) == 3:
                # Check for meshing.
                if set_ixa | set_ixb == set_ixb | set_ixc == set_ixc | set_ixa:
                    logging.debug("Swordfish found.")
                    stored = clean.copy()
                    # Clean on transverse axis.
                    for index in combined_set:
                        clean[index] &= not_mark
                    # Re-insert swordfish members.
                    for line_number, indices in (a, b, c):
                        for index in indices:
                            clean[index, line_number] |= mark
                    if (clean != stored).any():
                        logging.debug("Swordfish progress.")
                        return True
        return False


class XYZWing(PencilMethod):
    """XYZ-wing is similar to Y-wing but the hinge has three digits.

    Each hinge digit can be the digit of elimination if appropriate wings
    can be found. The digit of elimination is shared among the wings with
    the remaining two on each wing singly.

    Any cell seen by the hinge and both the wings can be eliminated of
    the common value."""

    def __call__(self) -> int:
        logging.debug("* XYZ-Wing *")

        grid = self.grid.grid
        prior_grid = grid.copy()

        hinge_locations = np.nonzero(np.isin(grid, self.grid.TRIPLES))

        for row, col in zip(*hinge_locations):
            xyz_marks = grid[row, col]
            hinge_digits = Grid.pencil_values(xyz_marks)
            hinge_map = point_influence(row, col)
            for z_digit in hinge_digits:
                x_digit, y_digit = set(hinge_digits) - set([z_digit])
                xz_marks = (1 << x_digit) | (1 << z_digit)
                yz_marks = (1 << y_digit) | (1 << z_digit)
                xz_wings = cast(pos_2d, np.nonzero((grid == xz_marks)
                                & hinge_map))
                yz_wings = cast(pos_2d, np.nonzero((grid == yz_marks)
                                & hinge_map))
                if xz_wings[0].size and yz_wings[0].size:
                    xz_map = points_influence(xz_wings)
                    yz_map = points_influence(yz_wings)
                    np.bitwise_and(grid, ~(1 << z_digit), out=grid,
                                   where=xz_map & yz_map & hinge_map)
                    if progress := np.flatnonzero(grid != prior_grid).size:
                        return progress

        return 0


@dataclass
class Candidate:
    mark: int
    grid: Grid


class BUG(PencilMethod):
    """Bi-value Universal Grave. This solves the BUG+1 pattern only.

    BUG+2 is where there are two triples. So with BUG there are only
    bi-value cells on the grid and either the grid is broken or there are
    two solutions. With BUG+1 there will be a digit in the triple that sees
    4 of its kind while the other two digits only see two of theirs. The odd
    one will be the solution provided the grid is unique otherwise there are
    three solutions.
    """

    def __call__(self) -> int:
        logging.debug("* BUG *")

        grid = self.grid.grid
        whats_left = grid & grid - 1
        singles = 81 - np.count_nonzero(whats_left)
        whats_left = whats_left & whats_left - 1
        doubles = 81 - singles - np.count_nonzero(whats_left)
        triple_locs = np.nonzero(whats_left)
        whats_left = whats_left & whats_left - 1
        triples = 81 - singles - doubles - np.count_nonzero(whats_left)

        if triples != 1 or doubles + singles != 80:
            return 0
        triple_loc = triple_locs[0][0], triple_locs[1][0]
        values = Grid.pencil_values(grid[triple_loc])
        logging.debug(f"BUG potential solutions {values}")
        candidates: dict[int, list[Candidate]] = defaultdict(list)
        for value in values:
            mark = 1 << value
            n_eliminations = np.count_nonzero((grid & mark == mark) &
                                              point_influence(*triple_loc))
            if n_eliminations not in (2, 4):  # Broken grid with no solutions?
                return 0
            grid_copy = self.grid.copy()
            grid_copy.grid[triple_loc] = mark
            candidates[n_eliminations].append(Candidate(mark, grid_copy))

        # Check we have 2 candidates with 2 eliminations each.
        # Check we have 1 candidate with 4 eliminations.
        if len(candidates[2]) != 2 and len(candidates[4]) != 1:
            logging.debug("BUG candidates count unexpected result")
            return 0

        if self._solves(candidates[2][0].grid) or \
                self._solves(candidates[2][1].grid):
            logging.debug("BUG multiple solutions")
            return 0

        if not self._solves(candidates[4][0].grid):
            logging.debug("BUG no solutions")
            return 0

        grid[triple_loc] = candidates[4][0].mark
        return 1

    def _solves(self, grid: Grid) -> bool:
        sc = SinglesCleanup(grid)
        try:
            while sc():
                pass
            MarkCheck(grid)()
        except SudokuError:
            return False

        return grid.filled == 81
