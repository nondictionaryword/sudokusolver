"""Common code for methods.

Contains code for coloring and tracking that could be shared/generalized."""


from __future__ import annotations

__all__ = ["Color", "position", "conjugates", "ChainData", "chain_t",
           "MapBase", "LinkMap", "InfluenceMap", "pos_2d", "Chain",
           "get_strong_links", "extract_chain", "paint_chain",
           "point_influence", "points_influence", "points_familiarity"]

from typing import cast, Optional, Union
from collections import defaultdict
from collections.abc import Iterable, Iterator, Mapping
from enum import Enum, auto
from abc import ABCMeta, abstractmethod
import logging
import itertools

import numpy as np

from ...error import SudokuError
from ...grid import Grid


class Color(Enum):
    """Chosen for their short names."""

    UNSET = auto()
    RED = auto()
    BLUE = auto()

    def other(this: Color) -> Color:
        if this is cast(Color, this.UNSET):
            raise SudokuError("other color does not apply to Color.UNSET")
        return cast(Color, (this.RED if this is cast(Color, this.BLUE)
                            else this.BLUE))


position = tuple[int, int]  # Position by grid[row, column]
conjugates = set[position]


class ChainData:
    """Provides links and a color attribute to nodes of a chain."""

    def __init__(self, color: Optional[Color] = None) -> None:
        self._conjugates: conjugates = set()
        self._color: Color = Color.UNSET if color is None else color

    def __repr__(self) -> str:
        return f"color={self.color.name} conjugates={self.conjugates}"

    @property
    def conjugates(self) -> conjugates:
        return self._conjugates

    @property
    def color(self) -> Color:
        return self._color

    @color.setter
    def color(self, new_color: Color) -> None:
        if self.color is not Color.UNSET:
            raise SudokuError("attempt to change an already set color")
        if not isinstance(new_color, Color):
            raise SudokuError("new_color not a type of Color")
        self._color = new_color


chain_t = dict[position, ChainData]


class MapBase(Mapping[Color, np.ndarray], metaclass=ABCMeta):
    def __init__(self, dict_: Chain):
        self._maps = {Color.RED: self.make_map(Color.RED, dict_),
                      Color.BLUE: self.make_map(Color.BLUE, dict_)}

    def __getitem__(self, key: Color) -> np.ndarray:
        return self._maps[key].copy()

    def __iter__(self) -> Iterator[Color]:
        return iter(self._maps)

    def __len__(self) -> int:
        return len(self._maps)

    @abstractmethod
    def make_map(self, color: Color, chain: Chain) -> np.ndarray:
        pass


class LinkMap(MapBase):
    """Map of the locations of the links by color.

    Pass in a Color to see one of two possible correct value placements."""

    def make_map(self, color: Color, chain: Chain) -> np.ndarray:
        map_ = np.zeros((9, 9), dtype=np.bool_)
        for (row, col), chain_data in chain.items():
            if chain_data.color is color:
                map_[row, col] = True

        return map_


class InfluenceMap(MapBase):
    """Map of the cells that are affected by members of the chain by color.

    Pass in a Color to see which cells must be off if that color is correct."""

    def make_map(self, color: Color, chain: Chain) -> np.ndarray:
        map_ = np.zeros((9, 9), dtype=np.bool_)
        for (row, col), chain_data in chain.items():
            if chain_data.color is not color:
                continue

            map_[row] = True
            map_[:, col] = True
            map_[row//3 * 3:row//3 * 3 + 3, col//3 * 3: col//3 * 3 + 3] = True
            map_[row, col] = False

        return map_


pos_2d = tuple[np.ndarray, ...]


class Chain(Mapping[position, ChainData]):
    def __init__(self, dict_: chain_t):
        self._dict = dict_
        self._link_map = LinkMap(self)
        self._influence_map = InfluenceMap(self)

    def __getitem__(self, key: position) -> ChainData:
        return self._dict[key]

    def __iter__(self) -> Iterator[position]:
        return iter(self._dict)

    def __len__(self) -> int:
        return len(self._dict)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({repr(self._dict)})"

    @property
    def link_map(self) -> LinkMap:
        """Map of links of a specific color on the chain."""

        return self._link_map

    @property
    def influence_map(self) -> InfluenceMap:
        """Map of cells seen by links of a specific color on the chain."""

        return self._influence_map


def get_strong_links(grid: Grid) -> chain_t:
    """Collect information on all the strong links in a grid of a single digit.

    If strongly linked chains are required extract_chains can be called on the
    result.
    """

    raw_chains: chain_t = defaultdict(ChainData)
    indices: Union[np.ndarray, tuple[np.ndarray, ...]]

    logging.debug("grid of digit under test")
    logging.debug(grid.grid)

    logging.debug("row links")

    for i, row in enumerate(grid.rows):
        indices = np.flatnonzero(row)
        if indices.size == 2:
            raw_chains[i, indices[0]].conjugates.add((i, indices[1]))
            raw_chains[i, indices[1]].conjugates.add((i, indices[0]))
            logging.debug(f"conjugate pair row {i}, columns {indices}")

    logging.debug("column links")

    for i, col in enumerate(grid.cols):
        indices = np.flatnonzero(col)
        if indices.size == 2:
            raw_chains[indices[0], i].conjugates.add((indices[1], i))
            raw_chains[indices[1], i].conjugates.add((indices[0], i))
            logging.debug(f"conjugate pair column {i}, rows {indices}")

    logging.debug("box links")

    for box_row, box_col in itertools.product(range(3), range(3)):
        box = grid.boxes[box_row, box_col]
        indices = np.nonzero(box)
        if indices[0].size == 2:
            first = box_row*3 + indices[0][0], box_col*3 + indices[1][0]
            second = box_row*3 + indices[0][1], box_col*3 + indices[1][1]
            raw_chains[first].conjugates.add(second)
            raw_chains[second].conjugates.add(first)
            logging.debug(f"{first=}, {second=}")

    logging.debug(f"{raw_chains=}")
    return raw_chains


def extract_chain(raw_chains: chain_t) -> Iterable[Chain]:
    """Iterator for obtaining colorized hard linked chains on a single digit.

    We shall colorize a random element of a random chain then radiate
    alternating colors along the chain until all elements are colorized.
    Pull these colored elements out as their own separate chain.
    Repeat until no more links are left."""

    while raw_chains:
        key: position = next(iter(raw_chains.keys()))
        raw_chains[key].color = Color.RED
        paint_chain(raw_chains, key)
        chain = Chain({key: value for key, value in raw_chains.items()
                       if value.color != Color.UNSET})
        for key in chain.keys():  # Why is dict.purge not a thing?
            del raw_chains[key]
        yield chain


def paint_chain(raw_chains: chain_t, key: position) -> None:
    """Paint all elements on a chain that key is a member of.

    Other chains may exist and must not be painted at this point."""

    chain_data = raw_chains[key]
    other_color = chain_data.color.other()
    for other_key in chain_data.conjugates:
        other_chain_data = raw_chains[other_key]
        if other_chain_data.color is Color.UNSET:
            other_chain_data.color = other_color
            paint_chain(raw_chains, other_key)


def point_influence(row: int, col: int) -> np.ndarray:
    """Cells that a cell at a coordinate directly overshadows."""

    grid = np.zeros((9, 9), dtype=bool)
    grid[row] = True
    grid[:, col] = True
    grid[row//3 * 3:row//3 * 3 + 3, col//3 * 3: col//3 * 3 + 3] = True
    grid[row, col] = False
    return grid


def points_influence(points: pos_2d) -> np.ndarray:
    """Intersection of point influences."""

    accumulator = np.ones((9, 9), dtype=bool)
    for row, col in zip(*points):
        accumulator &= point_influence(row, col)
    return accumulator


def points_familiarity(points: pos_2d) -> np.ndarray:
    """Number of times each cell can see a member of the coordinate list."""

    layers: list[np.ndarray] = []
    for row, col in zip(*points):
        layers.append(point_influence(row, col))
        layers[-1][row, col] = True
    return np.sum(layers, axis=0, keepdims=True, dtype=int)
