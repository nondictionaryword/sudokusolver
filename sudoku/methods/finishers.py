"""Brute force methods that finish a grid.

These do not belong in the solver rotation since they are not cooperative.
Instead they exist as a last resort or to run through all possible solutions
to a non-proper Sudoku.

The techniques are numbered in ascending order of sophistication.
"""

from __future__ import annotations

__all__ = ["Backtracking1", "Backtracking2", "Backtracking3", "Backtracking4",
           "FinisherCallback"]

import logging
from collections import deque
from collections.abc import Callable
from abc import abstractmethod
from functools import wraps
from typing import Any, Optional, Union, Final, cast, Iterable

import numpy as np

from ..error import SudokuError
from ..grid import Grid
from ..solver import MethodIter
from . import Method, DigitsMethod, PencilMethod, MarkCheck
from . import core
from . import tough
from . import common


FinisherCallback = Callable[..., None]


class Finisher(Method):
    """Base class for methods that finish a grid.

    Such methods do not belong in a method rotation. The callback provides
    a means to output solutions of which there might be more than one.
    """

    class Breakout(Exception):
        pass

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._solution_count = 0
        self._stage_count = 0
        self.set_output_callback(self._default_callback)
        self.set_output_callback_user_data(None)
        self._initial_grid: Optional[np.ndarray] = None

    @abstractmethod
    def __call__(self) -> int:
        return 0

    def output_result(self):
        self._solution_count += 1
        if self._callback is not None:
            self._callback(self, self._user_data)

    def set_output_callback(self, callback: FinisherCallback) -> None:
        self._callback = callback

    def set_output_callback_user_data(self, user_data: Any) -> None:
        self._user_data = user_data

    @staticmethod
    def _default_callback(self, _) -> None:
        """Print our grid to console."""

        print(self.grid)
        print(f"Solution number {self.solution_count} found after "
              f"{self.stage_count} stages.")

    def new_stage(self):
        self._stage_count += 1

    @property
    def stage_count(self):
        return self._stage_count

    @property
    def solution_count(self):
        return self._solution_count


class DigitsFinisher(DigitsMethod, Finisher):
    pass


class PencilFinisher(PencilMethod, Finisher):
    pass


FinisherMethodType = Union[type[DigitsFinisher], type[PencilFinisher]]


class Backtracking1(DigitsFinisher):
    """Backtracking method.

    Always produces a result if one exists.
    Can be slow on tricky grids.
    Can handle non definitive grids with multiple solutions.
    Must be left to finish because speculative values are placed.

    This version will start in either the top left corner or the bottom
    right depending on the clue distribution of the first and last box.
    This can result in drastic improvement of a grid's the solve time.
    """

    def __call__(self) -> int:
        logging.debug(f"** {self.__class__.__name__} **")
        boxes = self.grid.boxes
        # Simple box coverage check to determine solve direction.
        if np.flatnonzero(boxes[0, 0]).size > np.flatnonzero(boxes[2, 2]).size:
            self._process()  # Solve in regular forwards direction.
        else:
            # Backwards is probably going to be quicker so do that.
            self._process(row=8, col=8, step=-1)
        return self.solution_count

    def _process(self, row: int = 0, col: int = 0, step: int = +1) -> None:
        """Brute force search using backtracking method."""

        self.new_stage()  # Just bumps the stage counter.
        grid = self.grid.grid
        if row in (-1, 9):
            # Grid should be completed correctly. Time to display it.
            self.output_result()
        elif grid[row, col] == 0:
            for val in self.grid.cell_constraints(row, col):
                grid[row, col] = val
                self._process(*divmod(row*9 + col + step, 9), step)
            grid[row, col] = 0
        else:
            # Handle clues by skipping to the next cell...
            self._process(*divmod(row*9 + col + step, 9), step)


class Backtracking2(DigitsFinisher):
    """Backtracking method.

    Always produces a result if one exists.
    Can be slow on tricky grids.
    Can handle non definitive grids with multiple solutions.
    Must be left to finish because speculative values are placed.

    This version pre-builds a list of cells to visit in order of decreasing
    coverage. Placing high coverage cells first yields a 10% speedup.
    Not great but good to have.

    Unlike Backtracking1, clue cells do not contribute to the stage count.
    """

    def __call__(self) -> int:
        logging.debug(f"** {self.__class__.__name__} **")
        # Rank cells by how constrained they are.
        ranks = np.fromfunction(np.vectorize(self.grid.constraint_rank),
                                self.grid.grid.shape, dtype=int)
        # Build list of cell coordinates to visit ranked by
        # level of constraint.
        queue = deque(np.transpose(np.concatenate([(ranks == x).nonzero()
                      for x in range(0, 9)], axis=1, dtype=int)))

        max_depth = len(queue)
        grid = self.grid.grid

        def process(depth: int = 0) -> None:
            self.new_stage()
            if depth == max_depth:
                self.output_result()
                return

            row, col = queue[-1]  # Use right hand value.
            queue.rotate(+1)
            for val in self.grid.cell_constraints(row, col):
                grid[row, col] = val
                process(depth + 1)
            grid[row, col] = 0  # Scrub clean as we backtrack.
            queue.rotate(-1)

        process()
        return self.solution_count


class Backtracking3(DigitsFinisher):
    """Backtracking method.

    Always produces a result if one exists.
    Some grids may present a minor challenge.
    Can handle non definitive grids with multiple solutions.
    Must be left to finish because speculative values are placed.

    This version fills in all cells that have a constraint rank of 8 on each
    turn whenever possible, otherwise it finds a cell with the highest
    constraint rank available and tries to place a value for it.
    """

    def __call__(self) -> int:
        logging.debug(f"** {self.__class__.__name__} **")
        self._process()
        return self.solution_count

    def _process(self) -> None:
        self.new_stage()
        rows: list[int] = []
        cols: list[int] = []
        best_rank = -1

        for row in range(9):
            for col in range(9):
                rank = self.grid.constraint_rank(row, col)
                if rank == 9:
                    return

                if rank > best_rank:
                    best_rank = rank
                    rows = []
                    cols = []

                if rank == best_rank:
                    rows.append(row)
                    cols.append(col)

        if best_rank == -1:
            self.output_result()
            return

        if best_rank == 8:
            # This is where the speed comes from.
            # Without this the performance would be horrible.
            for row, col in zip(rows, cols):
                try:
                    rslt = next(self.grid.cell_constraints(row, col))
                except StopIteration:
                    # If not all rank 8's go in we have a broken situation.
                    self.grid.grid[rows, cols] = 0
                    return
                self.grid.grid[row, col] = rslt
            self._process()
            self.grid.grid[rows, cols] = 0
        else:
            row, col = rows[-1], cols[-1]
            for val in self.grid.cell_constraints(row, col):
                self.grid.grid[row, col] = val
                self._process()
                self.grid.grid[row, col] = 0


def handle_success(inner):
    @wraps(inner)
    def wrapper(self) -> None:
        try:
            if self.grid.filled == 81:
                self.output_result()
                return
        except SudokuError as e:
            logging.debug(f"{type(e)}: {e}")
            return

        self.new_stage()  # Increment the stage counter.
        inner(self)
    return wrapper


class Backtracking4(PencilFinisher):
    """Backtracking method.

    Always produces a result if one exists.
    Expect sub 20 millisecond solve times on proper Sudokus.
    Can handle non definitive grids with multiple solutions.
    Must be left to finish because speculative values are placed.
    Very poor performance on empty grids compared to Backtracking1.

    This version uses a subset of pencil solve methods along with bifurcation.
    """

    T = list[type[PencilMethod]]

    # Quick to process. Can solve trivial grids in one step.
    # Rotten performance on 17 clue broken grids.
    FAST: Final[T] = [core.SinglesCleanup, core.HiddenSingles,
                      tough.XWing, MarkCheck]

    # Slow to process but more likely to reveal grid inconsistencies.
    # Increased chance of generating bi-value cells.
    THOROUGH: Final[T] = [getattr(core, x) for x in core.SOLVE_METHODS] + \
                         [tough.XWing, MarkCheck]
    del T

    def __call__(self) -> int:
        logging.debug(f"** {self.__class__.__name__} **")
        self._solution_count = 0
        self._stage_count = 0
        self._initial_grid = self.grid.grid.copy()
        self._fast_method_set = [x(self.grid) for x in self.FAST]
        self._thorough_method_set = [x(self.grid) for x in self.THOROUGH]

        try:
            self._eliminate_marks()
        except self.Breakout:
            pass  # Grid expected to be dirty after Breakout exception.
        else:
            if (self.grid.grid != self._initial_grid).any():
                raise RuntimeError(f"Grid dirty after backtracking.")
        finally:
            # Want cleanup after a SudokuError from perhaps an unsolvable grid.
            np.copyto(self.grid.grid, self._initial_grid)
        return self.solution_count

    @handle_success
    def _eliminate_marks(self) -> None:
        """Apply pencil mark elimination methods."""

        orig: np.ndarray = self.grid.grid.copy()
        try:
            # MethodIter has the solve path reset whenever any method in the
            # seqence makes progress. It yields metadata we don't need to see.
            if self.grid.filled >= 25:
                for _ in MethodIter(self._fast_method_set):
                    pass
            if self.grid.filled < 60 and self.grid.bi_value_cells() == 0:
                for _ in MethodIter(self._thorough_method_set):
                    pass
            self._bifurcate()
        except SudokuError as e:
            # The grid broke, hopefully due to a bad bifurcation path.
            logging.debug(f"{type(e)}: {e}")
        np.copyto(self.grid.grid, orig)

    @staticmethod
    def _peel(x: np.ndarray) -> np.ndarray:
        return x & x - 1

    @handle_success
    def _bifurcate(self) -> None:
        """Place speculative solutions for a cell.

        Attempts to bifurcate on bi-value cells that see many other bi-values.
        """

        grid: np.ndarray = self.grid.grid
        peel_1 = self._peel(grid)
        peel_x = self._peel(peel_1)
        while not (choices := np.logical_xor(peel_x, peel_1)).any():
            # If here we are doing trifurcation (or worse) which is rare.
            peel_x = self._peel(peel_x)
        # Find the most highly associative bi-value cell.
        familiarity = common.points_familiarity(choices.nonzero())[0]
        np.copyto(familiarity, 0, where=~choices)
        row, col = cast(Iterable[int], np.unravel_index(familiarity.argmax(),
                                                        shape=(9, 9)))

        # Now we have selected the best cell, the speculative part.
        orig = grid[row, col]
        for solution in self.iterbits(grid[row, col]):
            grid[row, col] = solution
            self._eliminate_marks()
        grid[row, col] = orig
