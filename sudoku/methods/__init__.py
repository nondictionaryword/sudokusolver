"""Base classes of Methods."""


from __future__ import annotations

__all__ = ["Method", "DigitsMethod", "PencilMethod", "MethodIter", "MarkCheck"]

from typing import Any, cast, Iterable, NewType, Final
from abc import ABCMeta, abstractmethod
from collections.abc import Iterator
from functools import wraps
import logging

import numpy as np

from sudoku.error import SudokuError
from sudoku.grid import Mode, Grid, PencilMode


class MethodMeta(type, metaclass=ABCMeta):
    """Auto check correct Grid mode and detect retrograde actions."""

    def __new__(meta: type, name: str, bases: tuple[type, ...],
                dict_: dict[str, Any]) -> Any:

        if bases and "__call__" in dict_:
            __call__ = dict_["__call__"]

            @wraps(__call__)
            def wrapper(self: Method) -> int:
                self.grid.mode = self.mode
                if self.mode == Mode.DIGITS:
                    digits = self.grid.get_digits()
                if self.mode == Mode.PENCIL:
                    marks = self.grid.get_marks()
                ret: int = __call__(self)
                self.progress = ret
                if self.mode == Mode.DIGITS:
                    if not self.grid.verify_digits(digits):
                        raise SudokuError(f"{name} altered a clue.")
                if self.mode == Mode.PENCIL:
                    status, reason = self.grid.verify_marks(marks)
                    if not status:
                        raise SudokuError(f"pencil mark issue following "
                                          f"{name}: {reason}.")
                return ret

            dict_["__call__"] = wrapper

        return super(MethodMeta, cast(Any, meta)
                     ).__new__(meta, name, bases, dict_)


class Method(metaclass=MethodMeta):
    DEPENDENCIES: list[str] = []

    def __init__(self, grid: Grid):
        super().__init__()
        if not isinstance(grid, Grid):
            raise TypeError("Method expected a type of Grid.")
        self.__grid: Grid = grid
        self.reset()

    def reset(self) -> None:
        self.__progress = 0

    @abstractmethod
    def __call__(self) -> int:
        """Return 0 should the method fail to make progress.

        Method should return the correct amount of progress made
        i.e. number of cells that differ."""
        return 0

    @property
    def grid(self) -> Grid:
        return self.__grid

    @property
    def progress(self) -> int:
        return self.__progress

    @progress.setter
    def progress(self, delta: int) -> None:
        if delta < 0:
            raise SudokuError("attempted reduction of progress counter")
        self.__progress += delta

    @property
    def summary(self) -> str:
        return f"{self.__class__.__name__}: {self.progress}"

    @property
    @abstractmethod
    def mode(self) -> Mode:
        pass


class DigitsMethod(Method):
    """Superclass of methods that work in digits mode."""

    @property
    def mode(self) -> Mode:
        return Mode.DIGITS


class PencilMethod(Method):
    """Superclass of methods that work in pencil mode."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.bitwise_count = np.vectorize(self.bitwise_count)
        self.methods = []

    def set_methods(self, methods):
        self.methods = methods

    @property
    def mode(self) -> Mode:
        return Mode.PENCIL

    @staticmethod
    def iterbits(val: int) -> Iterator[int]:
        """Iterate over each bit (or pencil mark).

        functools.reduce(operator.__or__, iterbits(val)) == val
        """

        while val:
            bit = val & (~val + 1)
            yield bit
            val ^= bit

    @staticmethod
    def bitwise_count(x: np.intp) -> np.intp:
        return np.intp(int(x).bit_count())


class MarkCheck(PencilMethod):
    """Fault finding technique.

    Check each row, column, and box for the placeability of *all* marks.
    If marks are not placeable it could indicate an unsolvable inital grid or
    erroneous pencil mark removal from a faulty method.
    """

    class Breakout(Exception):
        pass

    def __call__(self) -> int:
        logging.debug("** Mark Check **")

        grid = self.grid
        aspects = zip(grid.rows, grid.cols, grid.boxes.flat)
        for i, (row, col, box) in enumerate(aspects):
            self._process(row, f"row {grid.ROW_NAMES[i]}")
            self._process(col, f"column {grid.COLUMN_NAMES[i]}")
            self._process(box.flat, f"box {i + 1}")

        return 0

    @staticmethod
    def _bit_count(val: np.int64) -> int:
        """Crappy workaround while we wait for NumPy version 2"""

        return int(val).bit_count()

    def _process(self, group: np.ndarray, specifier: str) -> None:
        if np.bitwise_or.reduce(group) != self.grid.PENCIL_MASK:
            raise SudokuError(f"{specifier} completely missing a pencil mark")

        try:
            # Ascending bit count order reduces the number of nodes generated.
            # Time is saved so long as the sort is fast enough.
            self._recursive(1, 0, np.array(sorted(group, key=self._bit_count)))
        except self.Breakout:
            logging.debug(f"{specifier} OK")
        else:
            txt = f"{self.__class__.__name__} {specifier} " \
                  f"no resolution pattern"
            logging.debug(txt)
            formatter: Any = {'int': self.grid.str_pencil_values}
            with np.printoptions(formatter=formatter, linewidth=160):
                logging.debug(str(group))
            raise SudokuError(txt)

    def _recursive(self, depth: int, marks: int, group: np.ndarray) -> None:
        if depth == 10:
            if marks == self.grid.PENCIL_MASK:
                raise self.Breakout("full mark assignment")
            return

        for mark in self.iterbits(group[depth - 1]):
            if not mark & marks:
                self._recursive(depth + 1, mark | marks, group)


class MethodIter:
    """Iterator for solve methods that are not finishers.

    Restarts the list when a method makes progress. Finishes when the list
    has been completed with no progress on the final pass.

    The supplied list needs to be in descending order of priority."""

    def __init__(self, iterable: Iterable[Method]):
        self._collection: tuple[Method, ...] = tuple(iterable)
        self._iter: Iterator[Method] = iter(self._collection)
        self._count: int = 0
        self._position: int = 0

    def __iter__(self) -> Iterator[tuple[int, int, Method]]:
        return self

    def __next__(self) -> tuple[int, int, Method]:
        method = next(self._iter)
        if self._position == 0 and method.grid.filled == 81:
            with PencilMode(method.grid):
                try:
                    # MarkCheck on a correctly filled grid should be fast.
                    MarkCheck(method.grid)()
                except SudokuError:
                    raise
                else:
                    raise StopIteration
        rslt: int = method()
        self._position += 1
        if rslt:
            self._iter = iter(self._collection)
            self._count += 1
            self._position = 0
        return self._count, rslt, method
