"""Standard eyeballing methods that do not involve pencil marks.

The core methods replicate what these do with the added advantage that they
also clear away pencil marks. These methods are ideally just used on their
own for very easy puzzles for this very reason."""


from __future__ import annotations

__all__ = ["Scanning", "NakedSingles"]

import logging
import itertools
from typing import Final

import numpy as np
import numpy.ma as ma

from . import DigitsMethod
from ..grid import Grid


SOLVE_METHODS: Final[list[str]] = __all__


class Scanning(DigitsMethod):
    """Pure scanning technique.

    Solved cells mask on their row column, and box. Pointing pairs and
    triples when detected do the same on the rest of their row or column.

    The value under test is placed in all unmasked and empty cells provided
    they are the only ones in their row, column, or box.
    """

    MASK: Final[int] = 16

    def __call__(self) -> int:
        """Iterate over each row, column and cell.

        Keep going as long as progress is being made."""

        logging.debug("** Box Line Scanning **")

        self._solutions: int = 0
        for value in range(10):
            old_grid = None
            while (self.grid.grid != old_grid).any():
                old_grid = self.grid.grid.copy()
                for row, name in zip(self.grid.rows, self.grid.ROW_NAMES):
                    self._solve_line(row, f"row {name}", value)

                for col, name in zip(self.grid.cols, self.grid.COLUMN_NAMES):
                    self._solve_line(col, f"column {name}", value)

                for box_row, box_col in itertools.product(range(3), repeat=2):
                    self._solve_box(box_row, box_col, value)

            self._clear_mask()

        return self._solutions

    def _solve_line(self, line: np.ndarray, where: str, value: int) -> None:
        """Apply mask if value is found. Place value in sole candidate cells.

        Mask is not applied to the cell containing value otherwise
        the value would not be available to trigger other masking operations.

        Mask application to newly inserted values occurs on the next pass.
        """

        if value in line:
            np.putmask(line, line != value, line | self.MASK)
        else:
            zero_indices = np.flatnonzero(line == 0)
            if len(zero_indices) == 1:  # Check for single zero.
                line[zero_indices] = value
                self._solutions += 1
                logging.debug(f"{value} at {where}.")

    def _solve_box(self, box_row: int, box_col: int, value: int) -> None:
        """Apply mask if value is found. Place value in sole candidate cells.

        Mask is not applied to the cell containing value otherwise
        the value would not be available to trigger other masking operations.

        Mask application to newly inserted values occurs on the next pass.

        Pointing pairs/triples mask the remainder of their line.
        """

        box: np.ndarray = self.grid.boxes[box_row, box_col]
        if value in box:
            np.putmask(box, box != value, box | self.MASK)
        else:
            box_rows, box_cols = np.nonzero(box == 0)
            if len(box_rows) == len(box_cols) == 1:  # Sole candidate.
                box[box_rows[0], box_cols[0]] = value
                self._solutions += 1
                logging.debug(f"{value} in box {box_row*3 + box_col + 1}")
            else:
                # Apply mask for pointing doubles/triples.
                if len(box_rows) in (2, 3):
                    if self._array_uniform(box_rows):
                        # Mask on row and box.
                        grid_row = self.grid.rows[box_rows[0] + box_row*3]
                        grid_row |= self.MASK
                        box |= self.MASK

                    if self._array_uniform(box_cols):
                        # Mask on column and box.
                        grid_col = self.grid.cols[box_cols[0] + box_col*3]
                        grid_col |= self.MASK
                        box |= self.MASK

                    # Remove mask from the inappropriate cells.
                    for row, col in zip(box_rows, box_cols):
                        box[row, col] = 0

    def _array_uniform(self, line: np.ndarray) -> bool:
        """True if all members of a non-empty 1d np.ndarray are the same."""

        return np.array_equal(line, np.full_like(line, line[0]))

    def _clear_mask(self) -> None:
        """Unconditionally remove mask from entire grid."""

        grid = self.grid.grid  # Python cannot do this with a one-liner.
        grid &= ~self.MASK


class NakedSingles(DigitsMethod):
    """Fill in cells that are constrained to a single value.

    We will consider what is in the cell's row, column, and box.
    """

    def __call__(self) -> int:
        logging.debug("** Naked Singles **")

        solutions = 0
        grid = self.grid
        for row, col in itertools.product(range(9), repeat=2):
            if grid.grid[row, col] == 0:
                vals = tuple(grid.cell_constraints(row, col))
                if len(vals) == 1:
                    grid.grid[row, col] = vals[0]
                    solutions += 1
                    logging.debug(f"{vals[0]} at "
                                  f"{grid.cell_name(row, col)}")
        return solutions
