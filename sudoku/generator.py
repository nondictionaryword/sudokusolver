"""Grid generator functionality."""


from __future__ import annotations

__all__ = ["generator"]

import random
import logging
from typing import Final, Any
from collections import Counter

import numpy as np

from sudoku.error import SudokuError
from sudoku.grid import Grid, CellData, Mode
from sudoku.solver import SolverMain
from sudoku.methods import MarkCheck, PencilMethod
import sudoku.methods.core as core
import sudoku.methods.tough as tough
from sudoku.methods.finishers import Finisher, Backtracking4


def quit_on_second_solution(finisher: Finisher,
                            user_data: list[np.ndarray]) -> None:
    if finisher.solution_count == 1:
        user_data[0] = finisher.grid.grid.copy()

    if finisher.solution_count > 1:
        raise finisher.Breakout


def random_grid(min_fill: int = 17, max_fill: int = 35,
                max_empty_groups: int = 1,
                constrain_spread: bool = True,
                discard_trivial: bool = False,
                broken_17_clue: bool = False) -> Grid:
    """Produce a random grid.

    Optional values:
        min_fill: minimum number of digits or 17 whichever is larger
        max_fill: maximum number of digits from min_fill to 81
        max_empty_groups: maximum allowed empty rows+columns+boxes
                          this is applied after the initial 17 digits
        constrain_spread: start off with non random digits
        discard_trivial: try and make it tough for Backtracking4
        broken_17_clue issue grids with no solutions ignores min and max fill
    """

    test = "17 <= min_fill <= max_fill <= 81"
    if not eval(test):
        raise ValueError(f"parameter check: {test} failed")

    grid: Final[Grid] = Grid()
    grid.mode = Mode.PENCIL
    reveals: Final[np.ndarray] = np.zeros(shape=(9, 9), dtype=bool)
    singles_cleanup: Final[core.SinglesCleanup] = core.SinglesCleanup(grid)
    singles_cleanup_nd: Final[core.SinglesCleanup] = core.SinglesCleanup(grid)
    mark_check = MarkCheck(grid)
    solution_detector: Final[Backtracking4] = Backtracking4(grid)
    solution_detector.set_output_callback(quit_on_second_solution)
    solution: list[np.ndarray] = [grid.grid]
    solution_detector.set_output_callback_user_data(solution)
    solution_detector.methods.insert(-1, MarkCheck)

    def placeable(digit: int, row: int, col: int) -> bool:
        # Prevent repeat digit placement in a cell.
        if reveals[row, col]:
            return False

        # Prevent digit double placement in any group.
        while singles_cleanup():
            pass

        return bool(grid.grid[row, col] & (1 << digit))

    def placeable_somewhere(digit: int):
        return grid.grid[np.logical_and(grid.grid & (1 << digit), ~reveals)
                         ].any()

    if constrain_spread:
        tmp = list(range(1, 10)) * 2
        random.shuffle(tmp)
        first_17 = tmp[:-1]
    else:
        while 1:
            first_17 = [random.randint(1, 9) for _ in range(17)]
            if max(Counter(first_17).values()) > 9:
                # Handle situation where more than 9 of some unit are selected.
                continue
            break

    while 1:
        try:
            reveal_count: int = 0
            for digit in first_17:
                while 1:
                    row = random.randint(0, 8)
                    col = random.randint(0, 8)
                    if not placeable(digit, row, col):
                        if not placeable_somewhere(digit):
                            raise SudokuError("grid rejected: this is normal")
                        continue
                    grid.grid[row, col] = 1 << digit
                    reveals[row, col] = True
                    reveal_count += 1
                    break
            while singles_cleanup():
                pass

            if grid.empty_groups() > max_empty_groups:
                raise SudokuError("grid rejected: empty groups")

            mark_check()
            solution_detector()
            if solution_detector.solution_count == 0:
                if broken_17_clue:
                    return grid
                else:
                    raise SudokuError
            elif broken_17_clue:
                raise SudokuError("grid rejected: for not being broken")

            while 1:
                if reveal_count > max_fill:
                    raise SudokuError("grid rejected: excess clues")

                if solution_detector.solution_count == 1:
                    if reveal_count < min_fill:
                        # Additional digits can be revealed if needed.
                        np.copyto(grid.grid, solution[0])
                        hidden = np.dstack((~reveals).nonzero())[0]
                        for _ in range(min_fill - reveal_count):
                            i = random.randint(0, hidden.size//2 - 1)
                            row, col = hidden[i]
                            reveals[row, col] = True
                            hidden = np.concatenate((hidden[:i], hidden[i+1:]))
                    break
                elif solution_detector.solution_count > 1:
                    # Find a random unsolved cell and test to see if we can
                    # use at least one of its marks chosen at random as part
                    # of a grid solution and if not raise SudokuError.
                    rows, cols = (~reveals).nonzero()
                    index = random.randint(0, rows.size - 1)
                    row, col = rows[index], cols[index]
                    marks = list(PencilMethod.iterbits(grid.grid[row, col]))
                    random.shuffle(marks)
                    for mark in marks:
                        grid.grid[row, col] = mark
                        solution_detector()
                        if solution_detector.solution_count != 0:
                            while singles_cleanup_nd():
                                pass
                            reveals[row, col] = True
                            reveal_count += 1
                            break
                    else:
                        raise SudokuError("grid rejected: unplaceable cell")

            grid.grid[~reveals] = Grid.PENCIL_MASK
            if discard_trivial:
                solver = SolverMain(grid_copy := grid.copy())
                solver.add_methods(core)
                solver.add_methods(tough)
                solver.solve()
                summary = solver.summary_dict()
                del summary["SinglesCleanup"]
                del summary["HiddenSingles"]
                if not (any(summary.values()) or grid_copy.filled < 81):
                    raise SudokuError("grid rejected: too easy")
            return grid

        except SudokuError as e:
            logging.info(e)
            grid.clear()
            reveals.fill(False)
