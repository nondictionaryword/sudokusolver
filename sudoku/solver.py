"""Solver functionality.

The methods used to achieve a solution are in sudoku.methods.
"""

from __future__ import annotations

__all__ = ["SolverMain"]

import logging
from typing import Any, cast, Optional, Union, Callable
from collections import defaultdict
from collections.abc import Iterable, Iterator
from types import ModuleType

import numpy as np

from sudoku.error import SudokuError
from sudoku.grid import Mode, Grid, CellData
from .methods import Method, PencilMethod, MethodIter
from .methods.finishers import Finisher, FinisherCallback


class SolverMain:
    """Solver main process routine.

    Takes a grid and allows the user to apply whichever selection of methods
    they prefer. Methods reside in sudoku.methods)."""

    def __init__(self,
                 parameter: Optional[Union[Grid, Iterable[CellData]]] = None):

        self._methods: list[Method] = []
        self._finisher: Optional[Finisher] = None

        if isinstance(parameter, Grid):
            self.grid = parameter
        elif isinstance(parameter, Iterable):
            self.grid = Grid(parameter)
        elif parameter is None:
            self.grid = Grid()
        else:
            raise TypeError("Optional parameter not a Grid or Iterable.")

    def solve(self) -> None:
        # Try methods until fruitless.
        if self.grid is None:
            raise SudokuError("Grid is None.")

        if not self._methods and self._finisher is None:
            raise SudokuError("No solve methods.")

        self.reset_methods()

        logging.info(f"Initial grid with {self.grid.filled} clues.")
        logging.info(self.grid)
        logging.info(format(self.grid, "."))

        if self._methods:
            initially_filled = self.grid.filled

            for pass_, progress, method in MethodIter(self._methods):
                if progress:
                    logging.info(f"Pass {pass_}: {method.__class__.__name__}: "
                                 f"{progress} units of progress.")

            logging.info(self.grid)
            logging.info("Grid after optional methods exhausted.")
            difference = self.grid.filled - initially_filled
            logging.info(f"Discovered an additional {difference} cells.")

            if self.grid.filled != 81:
                for method in self._methods:
                    if isinstance(method, PencilMethod):
                        self.grid.mode = Mode.PENCIL
                        formatter: Any = {'int': Grid.str_pencil_values}
                        with np.printoptions(formatter=formatter,
                                             linewidth=160):
                            logging.info(self.grid.grid)
                        logging.info("Pencil marks.")
                        break
                else:
                    logging.info("No pencil marks.")

        self.grid.mode = Mode.DIGITS
        if not self.grid.sanity_check():
            raise SudokuError("Bad grid after optional methods.")

        if self._finisher is not None:
            pre_finish_state = self.grid.grid.copy()
            logging.info(f"{self._finisher.__class__.__name__} "
                         f"is finishing the grid. This may take a while...")
            solution_count = self._finisher()

            if solution_count == 0:
                logging.info("No solutions.")
            elif solution_count == 1:
                logging.info("The grid has exactly one solution.")
            else:
                logging.info(f"{solution_count} solutions.")

            self.grid.mode = Mode.DIGITS
            if (self.grid.grid != pre_finish_state).any():
                raise SudokuError("Finisher left the grid modified.")

    def add_method(self, method: type[Method]) -> None:
        """Add a method.

        Multiple methods of the same type are allowed.
        """

        try:
            if not issubclass(method, Method):
                raise SudokuError(f"{method} is not a subclass of Method")

            if issubclass(method, Finisher):
                raise SudokuError(f"{method.__name__} is unsuitable for the "
                                  "method rotation")

            if method.DEPENDENCIES:
                names = [x.__class__.__qualname__ for x in self._methods]
                for dep_name in method.DEPENDENCIES:
                    if dep_name not in names:
                        raise SudokuError(f"Cannot add method "
                                          f"'{method.__name__}' due to "
                                          f"missing dependency '{dep_name}'.")

            # self is passed for access to the grid which is kind of important
            self._methods.append(method(self.grid))
        except TypeError:
            raise SudokuError(f"Bad type {method} for add_method method.")

    def add_methods(self, module: ModuleType) -> None:
        """Add all the methods in a module.

        The module needs to have a SOLVE_METHODS attribute."""

        try:
            for method_name in getattr(module, "SOLVE_METHODS"):
                self.add_method(getattr(module, method_name))
        except AttributeError as exc:
            raise SudokuError("SOLVE_METHODS missing") from exc

    def remove_method(self, method: type[Method]) -> None:
        self._methods = [x for x in self._methods if not isinstance(x, method)]

    def set_finisher(self, method: type[Finisher],
                     methods: Optional[list[type[PencilMethod]]] = None
                     ) -> None:
        """Apply a finisher to finish the grid provided it has a solution.

        If using a PencilMethod then a custom methods list can be specified.
        """

        if not issubclass(method, Finisher):
            raise SudokuError("parameter method needs to be subclass of"
                              "Finisher")
        self._finisher = method(self.grid)
        if methods is not None and isinstance(self._finisher, PencilMethod):
            cast(PencilMethod, self._finisher).set_methods(methods)

    def set_finisher_callback(self, callback: FinisherCallback) -> None:
        if not isinstance(self._finisher, Finisher):
            raise SudokuError("finisher callback not set for grid")
        self._finisher.set_output_callback(callback)

    def set_finisher_callback_user_data(self, user_data: Any) -> None:
        if not isinstance(self._finisher, Finisher):
            raise SudokuError("finisher callback not set for grid")
        self._finisher.set_output_callback_user_data(user_data)

    def reset(self) -> None:
        self.reset_methods()
        self.grid.reset()

    def reset_methods(self) -> None:
        for method in self._methods:
            method.reset()
        if self._finisher is not None:
            self._finisher.reset()

    def summary(self) -> None:
        if not self._methods:
            logging.info("Method list is empty. Cannot produce summary.")
            return

        logging.info("Methods performed as follows:")
        for method in self._methods:
            logging.info(method.summary)

    def summary_dict(self) -> defaultdict:
        dict_: defaultdict[str, int] = defaultdict(int)

        for method in self._methods:
            name = method.__class__.__name__
            dict_[name] += method.progress
        return dict_

    def has_methods(self) -> bool:
        return bool(self._methods)

    @property
    def grid(self) -> Grid:
        return self._grid

    @grid.setter
    def grid(self, grid: Grid) -> None:
        if not isinstance(grid, Grid):
            raise SudokuError("incorrect data type")
        self.reset_methods()
        self._grid = grid
