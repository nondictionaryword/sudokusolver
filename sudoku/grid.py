"""Sudoku grid functionality.

Supports both pencil and digits mode and permits switching between the two.
Has checking facilities to help developers catch bugs."""


from __future__ import annotations

__all__ = ["Mode", "CellData", "Grid"]

import logging
from itertools import combinations
from typing import Optional, Union, Final, Any, TypeVar, cast
from dataclasses import dataclass
from collections.abc import Iterable, Iterator, Callable, Collection
from functools import wraps
from enum import Enum

import numpy as np
import numpy.ma as ma

from sudoku.error import SudokuError


class Mode(Enum):
    DIGITS = "cells contain 0 for empty or [1..9]"
    PENCIL = "cells contain one or more of 1<<value"


@dataclass
class CellData:
    row: int
    col: int
    value: int  # Unset status represented by a value of 0

    def __post_init__(self) -> None:
        if not 0 <= self.row < 9 or not 0 <= self.col < 9:
            raise ValueError("row and column values must be in range [0,8]")
        if not 0 <= self.value <= 9:
            raise ValueError("value must be in range [0,9]")


F = TypeVar('F', bound=Callable[..., Any])
Args = tuple[Any]
KWArgs = dict[str, Any]


def digits_only(inner: F) -> F:
    """Decorator for applying Grid.mode restrictions on Grid methods."""

    @wraps(inner)
    def wrapper(self: Grid, *args: Args, **kwargs: KWArgs) -> Any:
        if self.mode != Mode.DIGITS:
            raise SudokuError(f"{inner.__qualname__} called while in "
                              f"{self.mode}, but is intended for digits only.")

        return inner(self, *args, **kwargs)
    return cast(F, wrapper)


def pencil_only(inner: F) -> F:
    """Decorator for applying Grid.mode restrictions on Grid methods."""

    @wraps(inner)
    def wrapper(self: Grid, *args: Args, **kwargs: KWArgs) -> Any:
        if self.mode != Mode.PENCIL:
            raise SudokuError(f"{inner.__qualname__} called while in "
                              f"{self.mode}, but is intended for pencil only.")

        return inner(self, *args, **kwargs)
    return cast(F, wrapper)


class DigitsMode:
    """Context manager for ensuring a particular grid mode."""

    def __init__(self, grid: Grid):
        if not isinstance(grid, Grid):
            raise ValueError("expected a type of Grid")
        self._grid = grid

    def __enter__(self) -> None:
        self._old_mode = self._grid.mode
        self._grid.mode = Mode.DIGITS

    def __exit__(self, *args) -> None:
        self._grid.mode = self._old_mode


class PencilMode:
    """Context manager for ensuring a particular grid mode."""

    def __init__(self, grid: Grid):
        if not isinstance(grid, Grid):
            raise ValueError("expected a type of Grid")
        self._grid = grid

    def __enter__(self) -> None:
        self._old_mode = self._grid.mode
        self._grid.mode = Mode.PENCIL

    def __exit__(self, *args) -> None:
        self._grid.mode = self._old_mode


class Grid:
    """A new grid with optional initial data."""

    # Row and column naming convention as used on sudokuwiki.org.
    ROW_NAMES: Final[str] = "ABCDEFGHJ"
    COLUMN_NAMES: Final[str] = "123456789"

    REVERSE_PENCIL: Final[dict[int, int]] = {1 << x: x for x in range(1, 10)}
    SINGLES: Final[tuple[int, ...]] = tuple(REVERSE_PENCIL)
    PAIRS: Final[tuple[int, ...]] = tuple((a | b) for a, b in
                                          combinations(SINGLES, 2))
    TRIPLES: Final[tuple[int, ...]] = tuple((a | b | c) for a, b, c in
                                            combinations(SINGLES, 3))
    QUADS: Final[tuple[int, ...]] = tuple((a | b | c | d) for a, b, c, d in
                                          combinations(SINGLES, 4))
    PENCIL_MASK: Final[int] = sum(SINGLES)
    DIGIT_MASK: Final[int] = 0xF

    def __init__(self, cells: Optional[Iterable[CellData]] = None):
        def mk_box_array(aspect: np.ndarray) -> np.ndarray:
            boxes = np.empty((3, 3), dtype=np.dtype(np.ndarray))
            for i, band in enumerate(np.vsplit(aspect, 3)):
                boxes[i] = np.hsplit(band, 3)  # Unpacks 3 boxes

            return boxes

        self._mode: Mode = Mode.DIGITS
        self._grid = np.zeros((9, 9), dtype=int)
        self._rows = self._grid
        self._cols = self._grid.transpose((1, 0))
        self._marks = np.full((9, 9), self.PENCIL_MASK, dtype=int)
        self._row_boxes = mk_box_array(self._rows)
        self._col_boxes = mk_box_array(self._cols)

        if cells is not None:
            self.set_multiple(cells)

    def copy(self) -> Grid:
        """Return a copy of the current grid."""

        grid_copy = Grid()
        grid_copy.mode = self.mode
        np.copyto(grid_copy.grid, self.grid)
        np.copyto(grid_copy._marks, self._marks)
        return grid_copy

    @classmethod
    def from_str(cls, grid: Collection[Union[str, int]]) -> Grid:
        """Constructor that takes a flat grid of 81 characters.

        Could take a tuple/list of values despite its name."""

        if not isinstance(grid, Collection):
            raise SudokuError("User supplied grid not a collection type.")
        if len(grid) != 81:
            raise SudokuError("User supplied grid not 81 elements in size.")

        self = Grid()
        for i, value in enumerate(grid):
            row, col = divmod(i, 9)
            try:
                assert not isinstance(value, float)
                if isinstance(value, str) and value == ".":
                    value = "0"
                value = int(value)
            except (TypeError, AssertionError):
                raise SudokuError("Non integer in grid.")
            self.set(CellData(row, col, value))

        return self

    def __repr__(self) -> str:
        with DigitsMode(self):
            data = ",\n     ".join(f"CellData({r}, {c}, {self.grid[r, c]})"
                                   for r in range(9) for c in range(9)
                                   if self._grid[r, c])
            return f"Grid({data})"

    def __format__(self, format_spec="0"):
        """Return a string representation of the grid 81 characters long.

        Specifying a format_spec of "." will substitute . characters for
        unsolved values.
        """

        if format_spec and format_spec not in ("0", "."):
            raise SudokuError("Format spec needs to be '0' or '.' or empty")

        values = []
        for val in self.grid.flatten():
            if self.mode == Mode.PENCIL:
                try:
                    val = self.REVERSE_PENCIL[val]
                except KeyError:
                    val = 0
            if not val and format_spec == ".":
                val = "."
            else:
                val = chr(val + ord("0"))
            values.append(val)

        return "".join(values)

    def __str__(self) -> str:
        """Informal string representation of the grid."""

        iter_ = iter(self._grid.flatten())
        row_name = iter(self.ROW_NAMES)

        def digit_val() -> str:
            return str(next(iter_) or " ")

        def pencil_val() -> str:
            digits = self.pencil_values(next(iter_) & self.PENCIL_MASK)
            return " " if len(digits) != 1 else str(digits[0])

        def val() -> str:
            if self.mode == Mode.DIGITS:
                return digit_val()
            elif self.mode == Mode.PENCIL:
                return pencil_val()
            else:
                assert False, "Not implemented."

        def triple() -> str:
            return " " + val() + " " + val() + " " + val() + " \u2502"

        def row() -> str:
            return next(row_name) + " \u2502" + triple() + triple() + triple()

        def band() -> str:
            return "\n".join((row(), row(), row()))

        col = "    {} {} {}   {} {} {}   {} {} {}".format(*self.COLUMN_NAMES)
        top = "\u252C".join(("\u2500" * 7, ) * 3).join(("  \u250C", "\u2510"))
        bot = "\u2534".join(("\u2500" * 7, ) * 3).join(("  \u2514", "\u2518"))
        mid = "\u253C".join(("\u2500" * 7, ) * 3).join(("  \u251C", "\u2524"))
        return "\n".join((col, top, band(), mid, band(), mid, band(), bot))

    def _pencil_marks_str(self) -> str:
        """Nicely formatted representation of the pencil marks."""

        formatter: Final[Any] = {'int': self.str_pencil_values}
        with PencilMode(self), np.printoptions(formatter=formatter,
                                               linewidth=160):
            return str(self.grid)

    @property
    def grid(self) -> np.ndarray:
        return self._grid

    @property
    def rows(self) -> np.ndarray:
        return self._rows

    @property
    def cols(self) -> np.ndarray:
        return self._cols

    @property
    def boxes(self) -> np.ndarray:
        return self._row_boxes

    @property
    def row_boxes(self) -> np.ndarray:
        return self._row_boxes

    @property
    def col_boxes(self) -> np.ndarray:
        return self._col_boxes

    @property
    def marks(self) -> str:
        return self._pencil_marks_str()

    @property
    def mode(self) -> Mode:
        return self._mode

    @mode.setter
    def mode(self, mode: Mode) -> None:
        if mode not in Mode:
            raise SudokuError(f"Bad grid mode {mode}")

        if mode == self.mode:
            return  # No change.

        if mode == Mode.PENCIL and self.mode == Mode.DIGITS:
            self._mode_digits_to_pencil()

        if mode == Mode.DIGITS and self.mode == Mode.PENCIL:
            self._mode_pencil_to_digits()

        self._mode = mode

    @digits_only
    def _mode_digits_to_pencil(self) -> None:
        """Change to pencil mode from digits mode.

        Convert known digits to pencil marks use them to overwrite
        existing pencil marks.
        Write pencil marks into grid."""

        new_marks: np.ndarray = np.vectorize(lambda x: 1 << x & ~1)(self.grid)
        np.copyto(self._marks, new_marks, where=(new_marks != 0))
        np.copyto(self.grid, self._marks)
        logging.debug("Changed from digits to pencil mode.")

    @pencil_only
    def _mode_pencil_to_digits(self) -> None:
        """Change to digits mode from pencil mode.

        Save pencil marks.
        Make a grid of digits corresponding to any lone pencil marks.
        Copy that result back."""

        def to_digit(marks: int) -> int:
            if not marks:
                raise SudokuError("Cell with no marks not allowed.")

            marks &= self.PENCIL_MASK
            old_marks = marks
            marks &= marks - 1
            return 0 if marks else self.REVERSE_PENCIL[old_marks]

        np.copyto(self._marks, self.grid)
        new_digits = np.vectorize(to_digit)(self.grid)
        np.copyto(self.grid, new_digits)
        logging.debug("Changed from pencil to digits mode.")

    @pencil_only
    def empty_groups(self) -> int:
        """Return the number of empty rows, columns, and boxes."""

        grid: Grid = self.copy()
        np.copyto(grid._grid, grid.grid & (grid.grid - 1))
        count: int = 0

        for aspect in grid.rows, grid.cols, grid.boxes.flatten():
            for group in aspect:
                if group.all():
                    count += 1

        return count

    @pencil_only
    def bi_value_cells(self) -> int:
        """Return the number of cells that currently have two pencil marks."""

        def peel(x):
            return x & x - 1

        grid = self.grid & self.PENCIL_MASK
        if not grid.all():
            raise SudokuError("Cell with no marks not allowed.")
        peel_1 = peel(grid)
        return np.count_nonzero(np.logical_xor(peel_1, peel(peel_1)))

    @property
    def filled(self) -> int:
        if self.mode == Mode.DIGITS:
            return np.count_nonzero(self.grid & self.DIGIT_MASK)

        elif self.mode == Mode.PENCIL:
            grid = self.grid & self.PENCIL_MASK
            if not grid.all():
                raise SudokuError("Cell with no marks not allowed.")
            return 81 - int(np.count_nonzero(grid & grid - 1))
        else:
            assert False, "Not implemented."

    @digits_only
    def set(self, cell: CellData) -> None:
        if cell.value == self.grid[cell.row, cell.col]:
            return
        if cell.value == 0 or cell.value in \
                self.cell_constraints(cell.row, cell.col):
            self._grid[cell.row, cell.col] = cell.value
        else:
            raise SudokuError(f"{cell.value} placed at "
                              f"{self.cell_name(cell.row, cell.col)} "
                              f"violates rules of Sudoku.")

    def set_multiple(self, cells: Iterable[CellData]) -> None:
        for cell in cells:
            self.set(cell)

    def reset(self) -> None:
        self.clear()
        self.mode = Mode.DIGITS

    def clear(self) -> None:
        if self.mode == Mode.PENCIL:
            self._grid.fill(self.PENCIL_MASK)
            self._marks.fill(0)
        else:
            self._grid.fill(0)
            self._marks.fill(self.PENCIL_MASK)

    def cell_constraints(self, row: int, col: int) -> Iterator[int]:
        """Iterable of values that can be placed in a cell."""

        if self.mode == Mode.DIGITS:
            search_space = np.concatenate((
                self.rows[row], self.cols[col],
                self.box_at_cell(row, col).flatten())) & self.DIGIT_MASK

            for val in range(1, 10):
                if val not in search_space:
                    yield val

        elif self.mode == Mode.PENCIL:
            # Just reel off the pencil bits contained within the cell.
            marks = self.grid[row, col] & self.PENCIL_MASK
            while marks:
                old_marks = marks
                marks &= marks - 1
                yield marks ^ old_marks

        else:
            assert False, "Not implemented."

    @digits_only
    def constraint_rank(self, row: int, col: int) -> int:
        """How constrained is a cell?

        Returns a value from -1 to 9 where -1 means the cell is occupied,
        0 means the cell's row, column, and box are empty.
        9 means there is no value that can be placed in the cell.
        """

        if self.grid[row, col]:
            return -1

        return (np.trim_zeros(np.unique(np.concatenate((
            self.rows[row], self.cols[col],
            self.box_at_cell(row, col).flatten()))))).size

    def box_at_cell(self, row: int, col: int) -> np.ndarray:
        """Get copy of data in a box at given cell co-ordinates."""

        return cast(np.ndarray, self.boxes[row // 3, col // 3])

    @classmethod
    def cell_name(cls, row: int, col: int) -> str:
        """Human readable cell name."""

        return cls.ROW_NAMES[row] + cls.COLUMN_NAMES[col]

    @classmethod
    def pencil_values(cls, pencil: int) -> list[int]:
        """Return the values stored in a pencil mark."""

        values = []
        while pencil:
            prior_pencil = pencil
            pencil &= pencil - 1
            values.append(cls.REVERSE_PENCIL[pencil ^ prior_pencil])

        return values

    @staticmethod
    def str_pencil_values(pencil: int) -> str:
        """Pencil values as a linear formatted string."""

        rslt: list[str] = []
        for i in range(1, 10):
            pencil >>= 1
            if pencil & 1:
                rslt.append(str(i))
            else:
                rslt.append(".")
        return "".join(rslt)

    @digits_only
    def sanity_check(self) -> bool:
        for aspect in iter(self.rows), iter(self.cols), \
                        (x.flatten() for x in self.boxes.flatten()):
            for each in aspect:
                value, count = np.unique(each, return_counts=True)
                if value[np.logical_and(count > 1, value != 0)].size > 0:
                    return False
                if set(value).issubset(range(10)) is False:
                    return False
        return True

    @digits_only
    def full_grid_integrity_check(self) -> tuple[bool, str]:
        """True if grid is filled, correct, and pencil marks overlap."""

        if self.filled != 81:
            return False, "grid not full"

        if not self.sanity_check():
            return False, "failed digits sanity check"

        if not (np.vectorize(lambda x: 1 << x)(self.grid) & self._marks).all():
            return False, "failed due to missing pencil marks"

        return True, "all checks passed"

    @digits_only
    def get_digits(self) -> ma.masked_array:
        grid_ = self.grid & self.DIGIT_MASK
        return ma.array(grid_, mask=(grid_ == 0))

    @digits_only
    def verify_digits(self, digits: ma.masked_array) -> bool:
        """False if a previously set digit was alterered."""

        return bool((self.grid == digits).all())

    @pencil_only
    def get_marks(self) -> np.ndarray:
        return cast(np.ndarray, self.grid & self.PENCIL_MASK)

    @pencil_only
    def verify_marks(self, old_marks: np.ndarray) -> tuple[bool, str]:
        """Returns verification status and explanation for failure."""

        new_marks = self.grid & self.PENCIL_MASK

        if not new_marks.all():
            return False, "at least one cell is lacking pencil marks"

        if ((new_marks & old_marks) ^ new_marks).any():
            return False, "added pencil mark(s)"

        return True, "all checks passed"
