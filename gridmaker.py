#!/usr/bin/env python

"""Generate Sudoku grids for use as test data."""


import csv
import logging
import argparse
import sys

from sudoku.generator import random_grid

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                            prog="gridmaker.py",
                            description="This program generates Sudoku grids.")

    parser.add_argument("-v", "--verbose", action="store_true", help="""
                        verbose output in terminal if outfile is specified""")
    parser.add_argument("-t", "--discard-trivial", action="store_true",
                        help="""the vast majority of grids are trivial""")
    parser.add_argument("-q", "--quantity", action="store", default=3000,
                        help="""number of grids to produce""")
    parser.add_argument("-l", "--min-fill", action="store", default=17,
                        help="""degree of grid fullness: minimum 17""")
    parser.add_argument("-m", "--max-fill", action="store", default=35,
                        help="""degree of grid fullness: maximum 81""")
    parser.add_argument("-e", "--max-empty-groups", action="store", default=1,
                        choices=("0", "1", "2"),
                        help="""number of rows, columns, and boxes that
                                may be empty""")
    parser.add_argument("-c", "--constrain-spread", action="store_true",
                        help="""reduces the randomness of the digits by drawing
                                from an initial non random set""")
    parser.add_argument("-b", "--broken-17-clue", action="store_true",
                        help="""unsolvalbe grids can be used for debugging""")
    parser.add_argument("outfile", nargs="?", type=argparse.FileType("a"),
                        default=sys.stdout, help="""
                        output to a file rather than stdout--files are
                        appended to rather than overwritten""")
    args = parser.parse_args()

    try:
        qty = int(args.quantity)
        test = "0 <= qty <= 1000000"
        if not eval(test):
            raise ValueError(f"{test} failed")
    except ValueError:
        print(f"quantity must be a value in the range {test}", file=sys.stderr)
        exit(5)

    try:
        min_fill = int(args.min_fill)
        test = "17 <= min_fill <= 81"
        if not eval(test):
            raise ValueError(f"{test} failed")
    except ValueError:
        print(f"min-fill outside the range {test}", file=sys.stderr)
        exit(5)

    try:
        max_fill = int(args.max_fill)
        test = "min_fill <= max_fill <= 81"
        if not eval(test):
            raise ValueError(f"{test} failed")
    except ValueError:
        print(f"max-fill outside the range {test}", file=sys.stderr)
        exit(5)

    try:
        max_empty_groups = int(args.max_empty_groups)
        test = "0 <= max_empty_groups <= 2"
        if not eval(test):
            raise ValueError(f"{test} failed")
    except ValueError:
        print(f"max-empty-groups outside the range {test}", file=sys.stderr)
        exit(5)

    show_progress_counter = False
    if not args.outfile.isatty():
        if args.verbose:
            logging.basicConfig(level=logging.INFO, format="%(message)s")
        else:
            show_progress_counter = True

    writer = csv.writer(args.outfile, quoting=csv.QUOTE_NONE)
    try:
        for i in range(qty):
            grid = random_grid(min_fill=min_fill, max_fill=max_fill,
                               max_empty_groups=max_empty_groups,
                               constrain_spread=args.constrain_spread,
                               discard_trivial=args.discard_trivial,
                               broken_17_clue=args.broken_17_clue)
            writer.writerow([f"{grid:0}"])
            if show_progress_counter:
                print(f"{i + 1} of {qty}", end="\r", file=sys.stderr)
            else:
                logging.info(f"Grid {i+1} of {qty} stored.")
    except KeyboardInterrupt:
        pass
