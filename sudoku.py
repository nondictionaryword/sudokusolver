#!/usr/bin/env python

"""Example usage and testing area for the sudoku module."""


import logging
from collections.abc import Callable
from timeit import default_timer as timer

from sudoku.error import SudokuError
from sudoku.grid import Grid, CellData, Mode
from sudoku.solver import SolverMain
from sudoku.methods import Method, MarkCheck
import sudoku.generator
import sudoku.methods.finishers as finishers
import sudoku.methods.eyeball
import sudoku.methods.core as core
import sudoku.methods.tough as tough
import sudoku.methods.diabolical as diabolical


if __name__ == "__main__":
    def impossible() -> Grid:
        return Grid((CellData(0, 2, 5),
                    CellData(0, 4, 2),
                    CellData(0, 6, 6),
                    CellData(1, 1, 9),
                    CellData(1, 5, 4),
                    CellData(1, 7, 1),
                    CellData(2, 0, 2),
                    CellData(2, 3, 5),
                    CellData(2, 8, 3),
                    CellData(3, 2, 6),
                    CellData(3, 4, 3),
                    CellData(4, 3, 8),
                    CellData(4, 5, 1),
                    CellData(5, 4, 9),
                    CellData(5, 6, 4),
                    CellData(6, 0, 3),
                    CellData(6, 5, 2),
                    CellData(6, 8, 7),
                    CellData(7, 1, 1),
                    CellData(7, 3, 9),
                    CellData(7, 7, 5),
                    CellData(8, 2, 4),
                    CellData(8, 4, 6),
                    CellData(8, 6, 8)))

    def extreme() -> Grid:
        # BoxLineScanning and NakedSingles fail to dent this one.
        # SingleCandidates gets nowhere either.
        return Grid((CellData(0, 0, 5),
                    CellData(0, 3, 4),
                    CellData(0, 7, 9),
                    CellData(2, 1, 6),
                    CellData(2, 3, 2),
                    CellData(2, 5, 1),
                    CellData(3, 0, 4),
                    CellData(3, 1, 7),
                    CellData(3, 6, 5),
                    CellData(4, 7, 3),
                    CellData(5, 1, 8),
                    CellData(5, 2, 3),
                    CellData(5, 3, 6),
                    CellData(6, 0, 2),
                    CellData(6, 8, 6),
                    CellData(7, 1, 1),
                    CellData(7, 5, 5),
                    CellData(7, 6, 7),
                    CellData(8, 1, 9),
                    CellData(8, 5, 7),
                    CellData(8, 6, 2),
                    CellData(8, 8, 1)))

    def box_line_reduction() -> Grid:
        # Example that uses box line reduction.
        return Grid.from_str("016007803"
                             "000800000"
                             "070001060"
                             "048000300"
                             "600000002"
                             "009000650"
                             "060900020"
                             "000002000"
                             "904600510")

    def hard_for_backtracking() -> Grid:
        # Really taxing puzzle for backtracking algorithm.
        # Beaten by BoxLineScanning with NakedSingles.
        # Beaten by SingleCandidates with HiddenSingles.
        return Grid((CellData(1, 5, 3),
                    CellData(1, 7, 8),
                    CellData(1, 8, 5),
                    CellData(2, 2, 1),
                    CellData(2, 4, 2),
                    CellData(3, 3, 5),
                    CellData(3, 5, 7),
                    CellData(4, 2, 4),
                    CellData(4, 6, 1),
                    CellData(5, 1, 9),
                    CellData(6, 0, 5),
                    CellData(6, 7, 7),
                    CellData(6, 8, 3),
                    CellData(7, 2, 2),
                    CellData(7, 4, 1),
                    CellData(8, 4, 4),
                    CellData(8, 8, 9)))

    def easy() -> Grid:
        # This one can be solved by BoxLineScanning, NakedSingles,
        # and also SingleCandidates.
        return Grid((CellData(0, 0, 5),
                    CellData(0, 1, 3),
                    CellData(0, 4, 7),
                    CellData(1, 0, 6),
                    CellData(1, 3, 1),
                    CellData(1, 4, 9),
                    CellData(1, 5, 5),
                    CellData(2, 1, 9),
                    CellData(2, 2, 8),
                    CellData(2, 7, 6),
                    CellData(3, 0, 8),
                    CellData(3, 4, 6),
                    CellData(3, 8, 3),
                    CellData(4, 0, 4),
                    CellData(4, 3, 8),
                    CellData(4, 5, 3),
                    CellData(4, 8, 1),
                    CellData(5, 0, 7),
                    CellData(5, 4, 2),
                    CellData(5, 8, 6),
                    CellData(6, 1, 6),
                    CellData(6, 6, 2),
                    CellData(6, 7, 8),
                    CellData(7, 3, 4),
                    CellData(7, 4, 1),
                    CellData(7, 5, 9),
                    CellData(7, 8, 5),
                    CellData(8, 4, 8),
                    CellData(8, 7, 7),
                    CellData(8, 8, 9)))

    def needs_ywing() -> Grid:
        return Grid.from_str("309000400"
                             "200709000"
                             "087000000"
                             "750060230"
                             "600904008"
                             "028050041"
                             "000000590"
                             "000106007"
                             "006000104")

    def another_easy() -> Grid:
        # Here just to demo the alternate constructor.
        return Grid.from_str("906870340"
                             "051040706"
                             "400006000"
                             "068007003"
                             "302010687"
                             "100000000"
                             "000002409"
                             "700508162"
                             "209004038")

    def contains_naked_pairs() -> Grid:
        return Grid.from_str("251348796"
                             "000917204"
                             "007256000"
                             "000060832"
                             "000000070"
                             "008000900"
                             "000620008"
                             "800700000"
                             "002501640")

    def needs_xwing() -> Grid:
        return Grid.from_str("000400602"
                             "006000100"
                             "090500080"
                             "050300000"
                             "301206405"
                             "000007020"
                             "030002060"
                             "004000900"
                             "507009000")

    def needs_simple_coloring() -> Grid:
        return Grid.from_str("803950407"
                             "604030500"
                             "705040230"
                             "402093170"
                             "109020340"
                             "357104092"
                             "030470901"
                             "970315024"
                             "041009703")

    def needs_swordfish() -> Grid:
        return Grid.from_str("008009000"
                             "300057001"
                             "000100009"
                             "230000070"
                             "005406100"
                             "060000038"
                             "900003000"
                             "700840003"
                             "000700600")

    def needs_xyz_wing() -> Grid:
        return Grid.from_str("072000680"
                             "000700000"
                             "500016000"
                             "000028100"
                             "200371006"
                             "004560000"
                             "000130004"
                             "000007000"
                             "015000890")

    def needs_bug() -> Grid:
        return Grid.from_str("200400501"
                             "001038090"
                             "030000708"
                             "070002003"
                             "060090005"
                             "040000009"
                             "004000060"
                             "620300800"
                             "810047000")

    def needs_rectangles_elimination() -> Grid:
        return Grid.from_str("7..1.2..3"
                             ".2.....7."
                             "3.6...1.2"
                             "...3.8..6"
                             ".5.....9."
                             "8..2.9..."
                             "6.8...5.1"
                             ".3.....8."
                             "1..6.5..4")

    def needs_re_hard() -> Grid:
        return Grid.from_str(".6.79..4."
                             "...4.17.2"
                             "........."
                             "94.....76"
                             "..56.91.."
                             "28.....39"
                             "........."
                             "6.83.2..."
                             ".9..64.5.")

    def needs_xcycles() -> Grid:
        return Grid.from_str("....1..6."
                             "...6..4.."
                             ".5..4.3.7"
                             ".....8..9"
                             ".8.....25"
                             "9..2....."
                             "6.3.2..4."
                             "..8..6..."
                             ".4..5....")

    def too_few_clues() -> Grid:
        # Not enough clues to solve conclusively but not so many solutions
        # that we cannot handle them.
        return Grid((CellData(0, 0, 4),
                    CellData(0, 1, 3),
                    CellData(0, 3, 9),
                    CellData(0, 6, 7),
                    CellData(1, 5, 3),
                    CellData(1, 7, 8),
                    CellData(1, 8, 5),
                    CellData(2, 2, 1),
                    CellData(2, 4, 2),
                    CellData(3, 3, 5),
                    CellData(3, 5, 7),
                    CellData(4, 2, 4),
                    CellData(4, 6, 1),
                    CellData(5, 1, 9),
                    CellData(6, 0, 5),
                    CellData(6, 7, 7),
                    CellData(6, 8, 3),
                    CellData(7, 2, 2),
                    CellData(8, 4, 4),
                    CellData(8, 6, 8),
                    CellData(8, 8, 9)))

    def very_hard_puzzle() -> Grid:
        return Grid.from_str("000004007"
                             "000701080"
                             "702006100"
                             "500000604"
                             "070000050"
                             "901005008"
                             "007108900"
                             "090302000"
                             "600500000")

    def no_solutions() -> Grid:
        return Grid.from_str("300004007"
                             "000701080"
                             "702006100"
                             "500000604"
                             "070000050"
                             "901005008"
                             "007108900"
                             "090302000"
                             "600500000")

    def no_clues() -> Grid:
        return Grid()

    def do_grid(make_grid: Callable[..., Grid]) -> None:
        logging.basicConfig(level=logging.INFO, format="%(message)s")
        start = timer()

        main = SolverMain(make_grid())
        # main.set_finisher(finishers.Backtracking4)
        # main.add_method(MarkCheck)
        # main.add_methods(sudoku.methods.eyeball)
        main.add_methods(core)
        main.add_methods(tough)
        main.add_methods(diabolical)
        main.remove_method(tough.RectangleElimination)
        main.solve()
        main.summary()

        end = timer()
        print(f"time taken: {end - start} | "
              f"log level: {logging.getLevelName(logging.root.level)}")

    def output_and_store_result(solver, user_data: list[str]):
        """An alternative (to the default) finisher callback routine.

        User data can be anything e.g. an empty list for collecting the
        various solutions.
        """

        if solver.solution_count > 200:
            raise solver.Breakout("exceeded solution count limit")

        rslt = format(solver.grid, ".")  # Flat format with '.' for unsolved.
        user_data.append(rslt)
        print(f"{solver.__class__.__name__} {rslt}")

    def bench_bt() -> None:
        """Race two finishers against each other with a series of grids."""

        logging.basicConfig(level=logging.INFO, format="%(message)s")
        a = b = 0.0

        a_solv = finishers.Backtracking3
        b_solv = finishers.Backtracking4
        b_methods = [core.SinglesCleanup, core.HiddenSingles, tough.XWing,
                     MarkCheck]
        # b_methods = None
        """
        test_list = (needs_bug, needs_xyz_wing, needs_swordfish, needs_xwing,
                     needs_simple_coloring, needs_xwing, contains_naked_pairs,
                     another_easy, needs_ywing, easy, hard_for_backtracking,
                     box_line_reduction, extreme, impossible, too_few_clues)
        """

        # test_list = (no_solutions,)
        test_list = (too_few_clues,)
        # test_list = (hard_for_backtracking,)

        for num, test in enumerate(test_list, 1):
            a_results = []
            b_results = []
            a_good = True
            b_good = True

            print(f"Job {num} of {len(test_list)} | "
                  f"Grid name: {test.__name__}\n{str(test())}\n"
                  f"Shareable Grid Data {test():.}")

            a_main = SolverMain(test())
            a_main.set_finisher(a_solv)
            a_main.set_finisher_callback(output_and_store_result)
            a_main.set_finisher_callback_user_data(a_results)
            # a_main.add_methods(core)
            # a_main.add_methods(tough)
            start_a = timer()
            try:
                a_main.solve()
            except a_solv.Breakout as e:
                print(e)
                a_good = False
            end_a = timer()

            b_main = SolverMain(test())
            b_main.set_finisher(b_solv, methods=b_methods)
            b_main.set_finisher_callback(output_and_store_result)
            b_main.set_finisher_callback_user_data(b_results)
            # b_main.add_methods(core)
            # b_main.add_methods(tough)
            start_b = timer()
            try:
                b_main.solve()
            except b_solv.Breakout as e:
                print(e)
                b_good = False
            end_b = timer()

            a += end_a - start_a
            b += end_b - start_b

            if a_good and b_good:
                if len(a_results) > 1 or len(b_results) > 1:
                    print("sorting multiple results")
                    a_results.sort()  # Need to sort in order to compare.
                    b_results.sort()

                if a_results != b_results:
                    raise SudokuError("results differ")
                print("results match")
                if len(a_results) != len(set(a_results)):
                    raise SudokuError("duplication(s) detected in results")
                print(f"grid has {len(a_results)} unique result(s)")
            else:
                print("results cannot be compared: solution limit reached")

        print("\nSummary")
        print(f"Solvers a={a_solv.__name__}, b={b_solv.__name__}")
        print(f"Total times (seconds) {a=}, {b=} seconds.")
        a /= len(test_list)
        b /= len(test_list)
        if len(test_list) > 1:
            print(f"Per solver average times (seconds) {a=}, {b=} seconds.")

    def novel_grid() -> None:
        logging.basicConfig(level=logging.INFO, format="%(message)s")
        while 1:
            grid = sudoku.generator.random_grid()
            solver = SolverMain(grid)
            solver.add_methods(core)
            solver.add_methods(tough)
            solver.set_finisher(finishers.Backtracking4)
            solver.solve()
            summary = solver.summary_dict()

            del summary["SinglesCleanup"]  # These are super easy.
            del summary["HiddenSingles"]  # Not so difficult either.
            if any(summary.values() or grid.filled < 81):
                solver.summary()
                break
            else:
                logging.info("Discarding boring grid and going again.")

    do_grid(needs_xcycles)
    # bench_bt()
    # novel_grid()
